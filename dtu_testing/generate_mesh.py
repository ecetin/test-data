"""
for fine-tuning
"""

import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader
import argparse
import os, sys

sys.path.insert(1, os.getcwd())

import logging
import numpy as np
import cv2 as cv
import trimesh
from shutil import copyfile
from icecream import ic
from tqdm import tqdm
from pyhocon import ConfigFactory, HOCONConverter

from models.fields import SingleVarianceNetwork

from dtu_testing.test_trainer import TestTrainer

from models.rendering_network import GeneralRenderingNetwork
from models.gmflow.gmflow import GMFlow
from models.sparse_sdf_network import SparseSdfNetwork, FinetuneOctreeSdfNetwork

from test_data import DtuFit
# from data.bmvs import BMVS

from utils.training_utils import tocuda

from termcolor import colored

from datetime import datetime


def count_params(trainables):
    """Counts total number of parameters"""
    num_params = []
    for model in trainables:
        model_parameters = filter(lambda p: p.requires_grad, model.parameters())
        params = sum([np.prod(p.size()) for p in model_parameters])
        num_params.append(int(params))

    return num_params


class Runner:
    def __init__(self, conf_path, mode='train', is_continue=False,
                 train_from_scratch=False,
                 local_rank=0, checkpoint_path=None, CASE_NAME=None, SET_NAME=None,
                 visibility_beta=0.015, visibility_gama=0.010,
                 visibility_penalize_ratio=0.8, visibility_weight_thred=[0.7],
                 dataset_near=425, dataset_far=900, clip_wh=[0, 0]):

        # Initial setting
        self.device = torch.device('cuda:%d' % local_rank)
        self.num_devices = torch.cuda.device_count()

        self.logger = logging.getLogger('exp_logger')

        print(colored("detected %d GPUs" % self.num_devices, "red"))

        self.conf_path = conf_path
        self.conf = ConfigFactory.parse_file(conf_path)

        self.conf['general']['set'] = SET_NAME
        self.conf['general']['case'] = CASE_NAME
        self.conf['general']['base_exp_dir'] = self.conf['general']['base_exp_dir'].replace(
            "CASE_NAME", CASE_NAME).replace("SET_NAME", SET_NAME)
        self.conf['dataset']['test_scan_id'] = self.conf['dataset']['test_scan_id'].replace("CASE_NAME", CASE_NAME)
        self.conf['dataset']['near'] = dataset_near
        self.conf['dataset']['far'] = dataset_far
        self.conf['dataset']['test_clip_wh'] = clip_wh
        self.conf['train']['visibility_beta'] = visibility_beta
        self.conf['train']['visibility_gama'] = visibility_gama
        self.conf['train']['visibility_penalize_ratio'] = visibility_penalize_ratio
        self.conf['train']['visibility_weight_thred'] = visibility_weight_thred

        self.base_exp_dir = self.conf['general.base_exp_dir']
        print(colored("base_exp_dir:  " + self.base_exp_dir, 'yellow'))
        os.makedirs(self.base_exp_dir, exist_ok=True)
        self.iter_step = 0
        self.val_step = 0

        # Training parameters inherited from SparseNeuS
        self.batch_size = self.num_devices  # use DataParallel to warp
        self.validate_resolution_level = self.conf.get_int('train.validate_resolution_level')
        
        self.use_white_bkgd = self.conf.get_bool('train.use_white_bkgd')
        self.N_rays = self.conf.get_int('train.N_rays')

        # Warmup params for sdf gradient inherited from SparseNeuS
        self.anneal_start = self.conf.get_float('train.anneal_start', default=0)
        self.anneal_end = self.conf.get_float('train.anneal_end', default=0)

        self.is_continue = is_continue
        self.mode = mode
        self.model_list = []

        # Build MatchNeRF Networks 
        self.build_encoder(self.conf, device=self.device)
        # Build SparseNeuS Networks
        self.build_decoder(self.conf)
        
        num_params = count_params(
            [self.encoder, self.sdf_network, 
             self.variance_network, self.rendering_network]
        )
        print(colored(f"The model has {num_params} parameters", 'yellow'))  

        # Renderer model
        self.trainer = TestTrainer(
            self.rendering_network_outside,
            self.encoder,
            self.sdf_network,
            self.variance_network,
            self.rendering_network,
            **self.conf['model.trainer'],
            conf=self.conf,
        )

        self.data_setup()  # * data setup

        # Load checkpoint
        checkpoint_dir = '/'.join(self.base_exp_dir.split('/')[:-2])
        model_list_raw = os.listdir(os.path.join(checkpoint_dir, 'checkpoints'))
        model_list = []
        for model_name in model_list_raw:
            if model_name.startswith('ckpt'):
                if model_name[-3:] == 'pth':  # and int(model_name[5:-4]) <= self.end_iter:
                    model_list.append(model_name)
        model_list.sort()
        latest_model_name = model_list[-1]

        if latest_model_name is not None:
            self.logger.info('Find checkpoint: {}'.format(latest_model_name))
            self.load_checkpoint(checkpoint_dir, latest_model_name)

        self.trainer = torch.nn.DataParallel(self.trainer).to(self.device)

        self.trainer = self.trainer.to(self.device)


    def build_encoder(self, config, device):
        """Builds the MatchNeRF modules that are used for encoding"""
        print(colored("Building MatchNeRF encoder networks", 'yellow'))

        feat_enc = GMFlow(
            feature_channels=config['model.encoder.feature_channels'], 
            num_scales=config['model.encoder.num_scales'], 
            num_head=config['model.encoder.num_head'],
            attention_type='swin', 
            ffn_dim_expansion=config['model.encoder.ffn_dim_expansion'],
            feature_upsampler=config['model.encoder.feature_upsampler'],
            upsample_factor=config['model.encoder.upsample_factor'],
            num_transformer_layers=config['model.encoder.num_transformer_layers'],
            device=device)
        self.encoder = feat_enc.to(self.device)


    def build_decoder(self, config):
        """Builds the SparseNeus modules that are used for decoding"""
        print(colored("Building SparseNeus decoder networks", 'yellow'))
        self.num_lods = self.conf.get_int('model.num_lods')

        self.rendering_network_outside = None
        self.sdf_network = None
        self.variance_network = None
        self.rendering_network = None
        # self.pyramid_feature_network = None  # extract 2d pyramid feature maps from images, used for geometry
        # self.pyramid_feature_network_lod1 = None  # may use different feature network for different lod

        # * pyramid_feature_network
        # self.pyramid_feature_network = FeatureNet().to(self.device)
        self.sdf_network = SparseSdfNetwork(
            **config['model.sdf_network']).to(self.device)
        self.variance_network = SingleVarianceNetwork(
            **config['model.variance_network']).to(self.device)

        self.rendering_network = GeneralRenderingNetwork(
            **config['model.rendering_network']).to(self.device)


    def data_setup(self):
        """
        if use ddp, use setup() not prepare_data(),
        prepare_data() only called on 1 GPU/TPU in distributed
        :return:
        """
        dataset = DtuFit

        print(os.path.join(self.conf['dataset.testpath'], self.conf['general']['set']))

        self.test_dataset = dataset(
            root_dir=os.path.join(self.conf['dataset.testpath'], self.conf['general']['set']),
            split='test',
            scan_id=self.conf['dataset.test_scan_id'],
            N_rays=self.conf.get_int('train.N_rays'),
            img_wh=self.conf['dataset.test_img_wh'],
            clip_wh=self.conf['dataset.test_clip_wh'],
            n_views=self.conf.get_int('dataset.test_n_views'),
            train_img_idx=self.conf.get_list('dataset.train_img_idx', default=[]),
            test_img_idx=self.conf.get_list('dataset.test_img_idx', default=[]),
            near=self.conf.get_float('dataset.near'),
            far=self.conf.get_float('dataset.far')
        )

        self.test_dataloader = DataLoader(self.test_dataset,
                                          shuffle=False,
                                          num_workers=4 * self.batch_size,
                                          batch_size=self.batch_size,
                                          pin_memory=True,
                                          drop_last=True
                                          )
        self.test_dataloader_iterator = iter(self.test_dataloader)

    
    def load_state_dict(self, network, checkpoint, comment):
        if network is not None:
            try:
                pretrained_dict = checkpoint[comment]

                model_dict = network.state_dict()

                # 1. filter out unnecessary keys
                pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
                # 2. overwrite entries in the existing state dict
                model_dict.update(pretrained_dict)
                # 3. load the new state dict
                message = network.load_state_dict(pretrained_dict)
                print(colored(comment + " load successful", 'green'))
            except:
                print(colored(comment + " load fails", 'yellow'))


    def load_checkpoint(self, checkpoint_dir, latest_model_name):
        file = os.path.join(checkpoint_dir, 'checkpoints', latest_model_name)
        checkpoint = torch.load(file, map_location=self.device)

        self.load_state_dict(self.encoder, checkpoint, 'encoder')
        self.load_state_dict(self.rendering_network_outside, checkpoint, 'rendering_network_outside')
        self.load_state_dict(self.sdf_network, checkpoint, 'sdf_network')

        self.load_state_dict(self.variance_network, checkpoint, 'variance_network')
        self.load_state_dict(self.rendering_network, checkpoint, 'rendering_network')

        if self.is_continue:
            try:
                self.optimizer.load_state_dict(checkpoint['optimizer'])
            except:
                print(colored("load optimizer fails", "yellow"))
            self.iter_step = checkpoint['iter_step'] if 'iter_step' in checkpoint.keys() else 0
            self.val_step = checkpoint['val_step'] if 'val_step' in checkpoint.keys() else 0

        print(colored(f"Pretrained model loaded from {file}.", "green"))
        self.logger.info('End')


    def validate(self, idx=-1, resolution_level=-1):
        # validate image

        ic(self.iter_step, idx)
        self.logger.info('Validate begin')

        if idx < 0:
            idx = self.val_step
        self.val_step += 1
        try:
            batch = next(self.test_dataloader_iterator)
        except:
            self.test_dataloader_iterator = iter(self.test_dataloader)  # reset
            batch = next(self.test_dataloader_iterator)

        background_rgb = None
        if self.use_white_bkgd:
            background_rgb = torch.ones([1, 3]).to(self.device)

        batch['batch_idx'] = torch.tensor([x for x in range(self.batch_size)])

        self.trainer(
            batch,
            background_rgb=background_rgb,
            iter_step=self.iter_step,
            save_vis=True,
            mode='val',
        )

    def gen_video(self):

        batch = next(self.test_dataloader_iterator)

        background_rgb = torch.ones([1, 3]).to(self.device)

        batch['batch_idx'] = torch.tensor([x for x in range(self.batch_size)])

        batch = tocuda(batch, self.device)

        self.trainer.gen_video(
            batch,
            background_rgb=background_rgb
        )


if __name__ == '__main__':
    # torch.set_default_tensor_type('torch.cuda.FloatTensor')
    torch.set_default_dtype(torch.float32)
    FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
    logging.basicConfig(level=logging.INFO, format=FORMAT)

    parser = argparse.ArgumentParser()
    parser.add_argument('--conf', type=str, default='./confs/base.conf')
    parser.add_argument('--mode', type=str, default='test')
    parser.add_argument('--threshold', type=float, default=0.0)
    parser.add_argument('--is_continue', default=False, action="store_true")
    parser.add_argument('--train_from_scratch', default=False, action="store_true")
    parser.add_argument('--local_rank', type=int, default=0)
    parser.add_argument('--case_name', type=str, help='the input dtu scan')
    parser.add_argument('--set_name', type=str, default='set0')
    parser.add_argument('--checkpoint_path', type=str, help='the pretrained checkpoint of general model')
    ## perscene finetuning params
    parser.add_argument('--visibility_beta', type=float, default=0.015, help='used in occlusion-aware patch loss')
    parser.add_argument('--visibility_gama', type=float, default=0.010, help='used in occlusion-aware patch loss')
    parser.add_argument('--visibility_penalize_ratio', type=float, default=0.8,
                        help='used in occlusion-aware patch loss')
    parser.add_argument('--visibility_weight_thred', nargs='+', default=[0.7],
                        help='visibility_weight_thred')
    parser.add_argument('--near', type=float, default=425)
    parser.add_argument('--far', type=float, default=900)
    parser.add_argument('--clip_wh', nargs='+', help='clip image width and height', default=[0, 0])

    args = parser.parse_args()

    # torch.cuda.set_device(args.local_rank)
    torch.backends.cudnn.benchmark = True  # ! make training 2x faster

    runner = Runner(args.conf, args.mode, args.is_continue,
                    args.train_from_scratch,
                    args.local_rank, CASE_NAME=args.case_name, SET_NAME=args.set_name,
                    checkpoint_path=args.checkpoint_path,
                    visibility_beta=args.visibility_beta,
                    visibility_gama=args.visibility_gama,
                    visibility_penalize_ratio=args.visibility_penalize_ratio,
                    visibility_weight_thred=[float(x) for x in args.visibility_weight_thred],
                    dataset_near=args.near,
                    dataset_far=args.far,
                    clip_wh=[int(x) for x in args.clip_wh]
                    )

    if args.mode == "test":
        runner.validate()
    elif args.mode == "video":
        runner.gen_video()
    else:
        raise Exception("Invalid mode!")