
#!/bin/bash

sets=("set0" "set1")

# Loop over each folder in the directory
for set in "${sets[@]}"
do
    # 
    python ./dtu_testing/eval_dtu_python.py --set_name $set
done
