"""
Trainer for fine-tuning
"""
import os
import cv2 as cv
import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np
import logging
import mcubes
import trimesh
from icecream import ic
from models.render_utils import sample_pdf
from utils.misc_utils import visualize_depth_numpy

from utils.training_utils import tocuda, numpy2tensor

from models.projector import Projector

from models.rays import gen_rays_between

from models.sparse_neus_renderer import SparseNeuSRenderer

import pdb


class TestTrainer(nn.Module):
    """
    Trainer used for test
    """

    def __init__(self,
                 rendering_network_outside,
                 encoder,
                 sdf_network,
                 variance_network,
                 rendering_network,
                 n_samples,
                 n_importance,
                 n_outside,
                 perturb,
                 alpha_type='div',
                 conf=None,
                 device=None
                 ):
        super(TestTrainer, self).__init__()

        self.conf = conf
        self.base_exp_dir = conf['general.base_exp_dir']

        self.anneal_start = self.conf.get_float('train.anneal_start', default=0.0)
        self.anneal_end = self.conf.get_float('train.anneal_end', default=0.0)

        # network setups
        self.rendering_network_outside = rendering_network_outside
        self.encoder = encoder
        self.sdf_network = sdf_network

        # - warpped by ModuleList to support DataParallel
        self.variance_network = variance_network

        self.rendering_network = rendering_network

        self.n_samples = n_samples
        self.n_importance = n_importance
        self.n_outside = n_outside
        self.num_lods = conf.get_int('model.num_lods')  # the number of octree lods
        self.perturb = perturb
        self.alpha_type = alpha_type

        # - the two renderers
        self.sdf_renderer = SparseNeuSRenderer(
            self.rendering_network_outside,
            self.sdf_network,
            self.variance_network,
            self.rendering_network,
            self.n_samples,
            self.n_importance,
            self.n_outside,
            self.perturb,
            alpha_type='div',
            conf=self.conf)

        # sdf network weights
        self.sdf_igr_weight = self.conf.get_float('train.sdf_igr_weight')
        self.sdf_sparse_weight = self.conf.get_float('train.sdf_sparse_weight', default=0)
        self.sdf_decay_param = self.conf.get_float('train.sdf_decay_param', default=100)
        self.fg_bg_weight = self.conf.get_float('train.fg_bg_weight', default=0.00)
        self.bg_ratio = self.conf.get_float('train.bg_ratio', default=0.0)
        self.depth_weight = self.conf.get_float('train.depth_weight', default=0.0)

        # self.depth_criterion = DepthLoss()

        # - DataParallel mode, cannot modify attributes in forward()

        # - True for finetuning; False for general training
        self.if_fitted_rendering = self.conf.get_bool('train.if_fitted_rendering', default=False)
        self.prune_depth_filter = self.conf.get_bool('model.prune_depth_filter', default=False)

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


    def val_step(self, sample,
                 perturb_overwrite=-1,
                 background_rgb=None,
                 alpha_inter_ratio=0.0,
                 iter_step=0,
                 chunk_size=512,
                 save_vis=False,
                 ):
        # * only support batch_size==1
        # ! attention: the list of string cannot be splited in DataParallel
        batch_idx = sample['batch_idx'][0]
        meta = sample['meta'][batch_idx]  # the scan lighting ref_view info

        sizeW = sample['img_wh'][0][0]
        sizeH = sample['img_wh'][0][1]
        H, W = sizeH, sizeW

        partial_vol_origin = sample['partial_vol_origin'].to(self.device)  # [B, 3]
        near, far = sample['query_near_far'][0, :1].to(self.device), sample['query_near_far'][0, 1:].to(self.device)

        breakpoint()

        # the ray variables
        sample_rays = sample['rays']
        rays_o = sample_rays['rays_o'][0]
        rays_d = sample_rays['rays_v'][0]
        rays_ndc_uv = sample_rays['rays_ndc_uv'][0]

        imgs = sample['images'][0].to(self.device)
        intrinsics = sample['intrinsics'][0].to(self.device)
        intrinsics_l_4x = intrinsics.clone()
        intrinsics_l_4x[:, :2] *= 0.25
        w2cs = sample['w2cs'][0].to(self.device)
        c2ws = sample['c2ws'][0].to(self.device)
        proj_matrices = sample['affine_mats'].to(self.device)

        # render_img_idx = sample['render_img_idx'][0]
        # true_img = sample['images'][0][render_img_idx]

        # - the image to render
        scale_mat = sample['scale_mat'].to(self.device)  # [1,4,4]  used to convert mesh into true scale
        trans_mat = sample['trans_mat'].to(self.device)
        query_c2w = sample['query_c2w'].to(self.device)  # [1,4,4]
        query_w2c = sample['query_w2c'].to(self.device)  # [1,4,4]
        true_img = sample['query_image'][0]
        true_img = np.uint8(true_img.permute(1, 2, 0).cpu().numpy() * 255)

        depth_min, depth_max = near.cpu().numpy(), far.cpu().numpy()

        true_depth = sample['query_depth'] if 'query_depth' in sample.keys() else None
        if true_depth is not None:
            true_depth = true_depth[0].cpu().numpy()
            true_depth_colored = visualize_depth_numpy(true_depth, [depth_min, depth_max])[0]
        else:
            true_depth_colored = None

        ref_poses = {}
        ref_poses['extrinsics'] = w2cs[None, :-1, :, :]  # B, N, 3, 4
        ref_poses['intrinsics'] = intrinsics[None, :-1]  # B, N, 3, 3
        ref_poses['near_fars'] = sample['near_fars'][:, :-1]  # B, N, 2

        tgt_pose = {}
        tgt_pose['extrinsics'] = w2cs[None, -1, :, :]  # B, 3, 4
        tgt_pose['intrinsics'] = intrinsics[None, -1]  # B, 3, 3
        tgt_pose['near_fars'] = sample['near_fars'][:, -1]  # B, 2

        rays_o = rays_o.reshape(-1, 3).to(self.device).split(chunk_size)
        rays_d = rays_d.reshape(-1, 3).to(self.device).split(chunk_size)

        # - obtain conditional features    
        imgs = imgs[None, :self.conf['dataset.test_n_views']]
        with torch.no_grad():
            ref_feats_list = self.get_img_feat(imgs, attn_splits_list=self.conf['model.encoder.attn_splits_list'],   #NOTE: should check if imgs works here
                                           cur_n_src_views=self.conf['dataset.test_n_views'])       #NOTE: weird how +1 takes out of bond here whereas needed in training     

        out_rgb_fine = []
        out_normal_fine = []
        out_depth_fine = []

        # out_depth_fine_explicit = []
        if save_vis:
            for rays_o_batch, rays_d_batch in zip(rays_o, rays_d):

                # ****** lod 0 ****
                render_out = self.sdf_renderer.render(     #was sdf_renderer_lod0
                    rays_o_batch, rays_d_batch, near, far,
                    self.sdf_network,       #was _lod0
                    self.rendering_network, #was _lod0
                    background_rgb=background_rgb,
                    alpha_inter_ratio=alpha_inter_ratio,        #was _lod0
                    # * related to conditional feature
                    conditional_valid_mask_volume=None,       #NOTE:should be removed next, was con_valid_mask_volume_lod0
                    # * 2d feature maps
                    feature_maps=None,     #NOTE: pobably should remove, used to be geometry_feature_maps
                    color_maps=imgs,
                    img_wh=[sizeW, sizeH],
                    if_general_rendering=True,
                    if_render_with_grad=True,
                    ref_feats_list=ref_feats_list,
                    ref_poses=ref_poses,
                    tgt_pose=tgt_pose,      
                )

                feasible = lambda key: ((key in render_out) and (render_out[key] is not None))

                if feasible('depth'):
                    out_depth_fine.append(render_out['depth'].detach().cpu().numpy())

                # if render_out['color_coarse'] is not None:
                if feasible('color_fine'):
                    out_rgb_fine.append(render_out['color_fine'].detach().cpu().numpy())
                if feasible('gradients') and feasible('weights'):
                    if render_out['inside_sphere'] is not None:
                        out_normal_fine.append((render_out['gradients'] * render_out['weights'][:,
                                                                          :self.n_samples_lod0 + self.n_importance_lod0,
                                                                          None] * render_out['inside_sphere'][
                                                    ..., None]).sum(dim=1).detach().cpu().numpy())
                    else:
                        out_normal_fine.append((render_out['gradients'] * render_out['weights'][:,
                                                                          :self.n_samples_lod0 + self.n_importance_lod0,
                                                                          None]).sum(dim=1).detach().cpu().numpy())
                del render_out

            # - save visualization of lod 0

            self.save_visualization(true_img, true_depth_colored, out_depth_fine, out_normal_fine,
                                    query_w2c[0], out_rgb_fine, H, W,
                                    depth_min, depth_max, iter_step, meta, "val_lod0")

        # - extract mesh
        torch.cuda.empty_cache()
        self.validate_mesh(
            self.sdf_network,
            self.sdf_renderer.extract_geometry,
            conditional_volume=None,       #used to be con_volume_lod0
            threshold=0,
            ref_poses=ref_poses, color_maps=imgs, ref_feats_list=ref_feats_list,
            device=rays_o_batch.device,
            # occupancy_mask=con_valid_mask_volume_lod0[0, 0],
            mode='val_bg', meta=meta,
            iter_step=iter_step, scale_mat=scale_mat, trans_mat=trans_mat
        )
        torch.cuda.empty_cache()


    def save_visualization(self, true_img, true_colored_depth, out_depth, out_normal, w2cs, out_color, H, W,
                           depth_min, depth_max, iter_step, meta, comment, out_color_mlp=[]):
        if len(out_color) > 0:
            img_fine = (np.concatenate(out_color, axis=0).reshape([H, W, 3]) * 256).clip(0, 255)

        if len(out_color_mlp) > 0:
            img_mlp = (np.concatenate(out_color_mlp, axis=0).reshape([H, W, 3]) * 256).clip(0, 255)

        if len(out_normal) > 0:
            normal_img = np.concatenate(out_normal, axis=0)
            rot = w2cs[:3, :3].detach().cpu().numpy()
            # - convert normal from world space to camera space
            normal_img = (np.matmul(rot[None, :, :],
                                    normal_img[:, :, None]).reshape([H, W, 3]) * 128 + 128).clip(0, 255)
        if len(out_depth) > 0:
            pred_depth = np.concatenate(out_depth, axis=0).reshape([H, W])
            pred_depth_colored = visualize_depth_numpy(pred_depth, [depth_min, depth_max])[0]

        if len(out_depth) > 0:
            os.makedirs(os.path.join(self.base_exp_dir, 'depths_' + comment), exist_ok=True)
            if true_colored_depth is not None:
                cv.imwrite(
                    os.path.join(self.base_exp_dir, 'depths_' + comment,
                                 '{:0>8d}_{}.png'.format(iter_step, meta)),
                    np.concatenate(
                        [true_colored_depth, pred_depth_colored, true_img])[:, :, ::-1])
            else:
                cv.imwrite(
                    os.path.join(self.base_exp_dir, 'depths_' + comment,
                                 '{:0>8d}_{}.png'.format(iter_step, meta)),
                    np.concatenate(
                        [pred_depth_colored, true_img])[:, :, ::-1])
        if len(out_color) > 0:
            os.makedirs(os.path.join(self.base_exp_dir, 'synthesized_color_' + comment), exist_ok=True)
            cv.imwrite(os.path.join(self.base_exp_dir, 'synthesized_color_' + comment,
                                    '{:0>8d}_{}.png'.format(iter_step, meta)),
                       np.concatenate(
                           [img_fine, true_img])[:, :, ::-1])  # bgr2rgb

        if len(out_color_mlp) > 0:
            os.makedirs(os.path.join(self.base_exp_dir, 'synthesized_color_mlp_' + comment), exist_ok=True)
            cv.imwrite(os.path.join(self.base_exp_dir, 'synthesized_color_mlp_' + comment,
                                    '{:0>8d}_{}.png'.format(iter_step, meta)),
                       np.concatenate(
                           [img_mlp, true_img])[:, :, ::-1])  # bgr2rgb

        if len(out_normal) > 0:
            os.makedirs(os.path.join(self.base_exp_dir, 'normals_' + comment), exist_ok=True)
            cv.imwrite(os.path.join(self.base_exp_dir, 'normals_' + comment,
                                    '{:0>8d}_{}.png'.format(iter_step, meta)),
                       normal_img[:, :, ::-1])

    def forward(self, sample,
                perturb_overwrite=-1,
                background_rgb=None,
                alpha_inter_ratio=1.0,
                iter_step=0,
                mode='test',
                save_vis=False,
                ):
        return self.val_step(
            sample,
            perturb_overwrite=perturb_overwrite,
            background_rgb=background_rgb,
            alpha_inter_ratio=0.,
            iter_step=iter_step,
            save_vis=save_vis,
        )

    def gen_video(self, sample,
                  perturb_overwrite=-1,
                  background_rgb=None,
                  iter_step=0,
                  chunk_size=1024,
                  ):
        # * only support batch_size==1
        batch_idx = sample['batch_idx'][0]
        meta = sample['meta'][batch_idx]  # the scan lighting ref_view info

        sizeW = sample['img_wh'][0][0]
        sizeH = sample['img_wh'][0][1]
        H, W = sizeH, sizeW

        partial_vol_origin = sample['partial_vol_origin'].to(self.device)  # [B, 3]
        near, far = sample['query_near_far'][0, :1].to(self.device), sample['query_near_far'][0, 1:].to(self.device)

        imgs = sample['images'][0].to(self.device)
        intrinsics = sample['intrinsics'][0].to(self.device)
        intrinsics_l_4x = intrinsics.clone()
        intrinsics_l_4x[:, :2] *= 0.25
        w2cs = sample['w2cs'][0].to(self.device)
        c2ws = sample['c2ws'][0].to(self.device)
        proj_matrices = sample['affine_mats'].to(self.device)

        # render_img_idx = sample['render_img_idx'][0]
        # true_img = sample['images'][0][render_img_idx]

        # - the image to render
        scale_mat = sample['scale_mat'].to(self.device)  # [1,4,4]  used to convert mesh into true scale
        trans_mat = sample['trans_mat'].to(self.device)
        query_c2w = sample['query_c2w'].to(self.device)  # [1,4,4]
        query_w2c = sample['query_w2c'].to(self.device)  # [1,4,4]
        true_img = sample['query_image'][0]
        true_img = np.uint8(true_img.permute(1, 2, 0).cpu().numpy() * 255)

        ################################################################################
        ################################################################################
        ref_poses = {}
        ref_poses['extrinsics'] = w2cs[None, :-1, :, :]  # B, N, 3, 4
        ref_poses['intrinsics'] = intrinsics[None, :-1]  # B, N, 3, 3
        ref_poses['near_fars'] = sample['near_fars'][:, :-1]  # B, N, 2

        tgt_pose = {}
        tgt_pose['extrinsics'] = w2cs[None, -1, :, :]  # B, 3, 4
        tgt_pose['intrinsics'] = intrinsics[None, -1]  # B, 3, 3
        tgt_pose['near_fars'] = sample['near_fars'][:, -1]  # B, 2
        ################################################################################
        ################################################################################


        rendering_c2ws = sample['rendering_c2ws'][0]  # [n, 4, 4]
        rendering_imgs_idx = sample['rendering_imgs_idx'][0]

        depth_min, depth_max = near.cpu().numpy(), far.cpu().numpy()

        # - obtain conditional features    
        imgs = imgs[None, :self.conf['dataset.test_n_views']]
        with torch.no_grad():
            ref_feats_list = self.get_img_feat(imgs, attn_splits_list=self.conf['model.encoder.attn_splits_list'],   # NOTE: should check if imgs works here
                                           cur_n_src_views=self.conf['dataset.test_n_views'])       # NOTE: weird how +1 takes out of bond here whereas needed in training 

        # coords_lod1 = conditional_features_lod1['coords_scale1']  # [1,3,wX,wY,wZ]

        inter_views_num = 60
        resolution_level = 2
        for r_idx in range(rendering_c2ws.shape[0] - 1):
            for idx in range(inter_views_num):
                query_c2w, rays_o, rays_d = gen_rays_between(
                    rendering_c2ws[r_idx], rendering_c2ws[r_idx + 1], intrinsics[0],
                    np.sin(((idx / 60.0) - 0.5) * np.pi) * 0.5 + 0.5,
                    H, W, resolution_level=resolution_level)
                
                query_c2w = query_c2w.unsqueeze(0)

                rays_o = rays_o.reshape(-1, 3).split(chunk_size)
                rays_d = rays_d.reshape(-1, 3).split(chunk_size)

                out_rgb_fine = []
                out_normal_fine = []
                out_depth_fine = []

                for rays_o_batch, rays_d_batch in zip(rays_o, rays_d):
                    render_out = self.sdf_renderer.render(     #was sdf_renderer_lod0
                        rays_o_batch, rays_d_batch, near, far,
                        self.sdf_network,       #was _lod0
                        self.rendering_network, #was _lod0
                        background_rgb=background_rgb,
                        alpha_inter_ratio=1.,        #was _lod0
                        # * related to conditional feature
                        conditional_valid_mask_volume=None,       #NOTE:should be removed next, was con_valid_mask_volume_lod0
                        # * 2d feature maps
                        feature_maps=None,     #NOTE: pobably should remove, used to be geometry_feature_maps
                        color_maps=imgs,
                        img_wh=[sizeW, sizeH],
                        if_general_rendering=True,
                        if_render_with_grad=True,
                        ref_feats_list=ref_feats_list,
                        ref_poses=ref_poses,
                        tgt_pose=tgt_pose,      
                    )

                    feasible = lambda key: ((key in render_out) and (render_out[key] is not None))

                    if feasible('depth'):
                        out_depth_fine.append(render_out['depth'].detach().cpu().numpy())

                    # if render_out['color_coarse'] is not None:
                   
                    if feasible('color_fine'):
                        out_rgb_fine.append(render_out['color_fine'].detach().cpu().numpy())
                    if feasible('gradients') and feasible('weights'):
                        if render_out_lod1['inside_sphere'] is not None:
                            out_normal_fine.append((render_out['gradients'] * render_out['weights'][:,
                                                                                        :self.n_samples_lod1 + self.n_importance_lod1,
                                                                                        None] *
                                                         render_out['inside_sphere'][
                                                             ..., None]).sum(dim=1).detach().cpu().numpy())
                        else:
                            out_normal_fine.append((render_out['gradients'] * render_out['weights'][:,
                                                                                        :self.n_samples_lod1 + self.n_importance_lod1,
                                                                                        None]).sum(
                                dim=1).detach().cpu().numpy())
                    del render_out_lod1

                img_fine = (np.concatenate(out_rgb_fine, axis=0).reshape(
                    [H // resolution_level, W // resolution_level, 3, -1]) * 256).clip(0, 255)
                save_dir = os.path.join(self.base_exp_dir, 'render_{}_{}'.format(rendering_imgs_idx[r_idx],
                                                                                 rendering_imgs_idx[r_idx + 1]))
                os.makedirs(save_dir, exist_ok=True)
                # ic(img_fine.shape)
                print(cv.imwrite(
                    os.path.join(save_dir, '{}.png'.format(idx + r_idx * inter_views_num)),
                    img_fine.squeeze()[:, :, ::-1]))
                print(os.path.join(save_dir, '{}.png'.format(idx + r_idx * inter_views_num)))

    def validate_mesh(self, density_or_sdf_network, func_extract_geometry, world_space=True, resolution=360,
                      threshold=0.0, mode='val',
                      # * 3d feature volume
                      conditional_volume=None, lod=None, occupancy_mask=None,
                      bound_min=[-1, -1, -1], bound_max=[1, 1, 1], meta='', iter_step=0, scale_mat=None,
                      trans_mat=None
                      ):

        bound_min = torch.tensor(bound_min, dtype=torch.float32)
        bound_max = torch.tensor(bound_max, dtype=torch.float32)

        vertices, triangles, fields = func_extract_geometry(
            density_or_sdf_network,
            bound_min, bound_max, resolution=resolution,
            threshold=threshold, device=conditional_volume.device,
            # * 3d feature volume
            conditional_volume=conditional_volume, lod=lod,
            occupancy_mask=occupancy_mask
        )

        if scale_mat is not None:
            scale_mat_np = scale_mat.cpu().numpy()
            vertices = vertices * scale_mat_np[0][0, 0] + scale_mat_np[0][:3, 3][None]

        if trans_mat is not None:
            trans_mat_np = trans_mat.cpu().numpy()
            vertices_homo = np.concatenate([vertices, np.ones_like(vertices[:, :1])], axis=1)
            vertices = np.matmul(trans_mat_np, vertices_homo[:, :, None])[:, :3, 0]

        mesh = trimesh.Trimesh(vertices, triangles)
        os.makedirs(os.path.join(self.base_exp_dir, 'meshes_' + mode), exist_ok=True)
        mesh.export(os.path.join(self.base_exp_dir, 'meshes_' + mode,
                                 'mesh_{:0>8d}_{}_lod{:0>1d}.ply'.format(iter_step, meta, lod)))