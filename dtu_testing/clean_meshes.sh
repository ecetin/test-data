
#!/bin/bash

sets=("set0" "set1")

# Loop over each folder in the directory
for set in "${sets[@]}"
do
    # Clean meshes for each set
    python ./dtu_testing/clean_mesh.py --set_name $set
done
