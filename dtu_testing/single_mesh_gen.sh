
#!/bin/bash



python ./dtu_testing/generate_mesh.py \
    --mode test --conf ./dtu_testing/test_scan_onebyone.conf \
    --checkpoint_path ../exp/dtu/deneme4/checkpoints/ckpt_001500.pth \
    --case_name scan24 --set_name set0 --near 425 --far 900 \
    --visibility_beta 0.025 --visibility_gama 0.010 --visibility_weight_thred 0.7 0.6 0.5