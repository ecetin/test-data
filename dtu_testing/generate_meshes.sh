
#!/bin/bash

sets=("set0" "set1")

scans=(
    "scan24" "scan37" "scan40" "scan55" "scan63" "scan65" 
    "scan69" "scan83" "scan97" "scan105" "scan106" "scan110"
    "scan114" "scan118" "scan122")

# Loop over each folder in the directory
for set in "${sets[@]}"
do
    for scan in "${scans[@]}"
    do
        # Print the name of the folder
        # Do something with the folder, like list its contents
        python ./eren_test_using_generic/generate_mesh.py \
        --mode test --conf ./eren_test_using_generic/test_scan_onebyone.conf \
        --checkpoint_path /scratch_net/biwidl305/rl_course_04/MatchSDF/exp/dtu/generalized_lod1/checkpoints/ckpt_200000.pth \
        --case_name $scan --set_name $set --near 425 --far 900 \
        --visibility_beta 0.025 --visibility_gama 0.010 --visibility_weight_thred 0.7 0.6 0.5
    done
done

# --checkpoint_path ./exp/dtu/test_generic/checkpoints/ckpt_200000.pth \
        