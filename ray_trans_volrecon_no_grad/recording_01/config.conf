# - for the lod1 geometry network, using adaptive cost for sparse cost regularization network
#- for lod1 rendering network, using depth-adaptive render

general {
  base_exp_dir = ./exp/dtu/ray_trans_volrecon_no_grad
  recording = [
    ./,
    ./data
    ./ops
    ./models
    ./loss
  ]
}

dataset {
  # local path
  trainpath = /kuacc/users/ecetin17/mvs_training/dtu
  valpath = /kuacc/users/ecetin17/mvs_training/dtu
  testpath = ./DTU_IDR/DTU

  train_split = train

  imgScale_train = 1.0
  imgScale_test = 1.0
  nviews = 3
  clean_image = True
  importance_sample = True
  test_ref_views = [23]

  # test dataset
  test_n_views = 3
  test_img_wh = [800, 600]
  test_clip_wh = [0, 0]
  test_scan_id = scan110
  train_img_idx = [49, 50, 52, 53, 54, 56, 58] #[21, 22, 23, 24, 25]  #
  test_img_idx = [51, 55, 57]  #[32, 33, 34]  #

  test_dir_comment = test

  # Train only on one scan
  train_do_single_scan = false
  train_scan_id = 7 # Paris model house blocks = 19, bunny = 7

  # Validate only on one scan
  test_do_single_scan = false
  test_scan_id = 65 # Stonehenge = 40
}

train {
  learning_rate = 2e-4
  learning_rate_enc = 5e-5
  learning_rate_milestone = [100000, 150000, 200000]
  # learning_rate_milestone = [10000, 50000, 100000, 150000, 200000]
  learning_rate_factor = 0.5
  end_iter = 200000
  save_freq = 5000 # was 5000
  val_freq = 5000  # was 5000
  val_mesh_freq = 5000 # was 2000
  report_freq = 100   # was 100

  mesh_resolution = 128
  sampling_strategy = "rays" # "cubes" or "rays"
  
  # random ray generation
  rand_n_rays = 16
  rand_n_points = 64

  grid_sample_padding_mode = "border"  # try "zeros" maybe

  N_rays = 512

  validate_resolution_level = 4
  anneal_start = 0
  anneal_end = 25000
  anneal_start_lod1 = 0
  anneal_end_lod1 = 15000

  use_white_bkgd = False

  # Loss
  # ! for training the lod1 network, don't use this regularization in first 10k steps; then use the regularization
  sdf_igr_weight = 0.0 #0.1
  sdf_sparse_weight = 0.0 # 0.02 for lod1 network;  0.02 for lod0 network
  sdf_decay_param = 100     # cannot be too large, which decide the tsdf range
  fg_bg_weight = 0.0  # first 0.01
  bg_ratio = 0.3
  depth_weight = 1.0
  color_weight = 1.0  # first 0.1

  if_fix_lod0_networks = False
}

model {
  num_lods = 1
  which_feat_info = ["new_feat", "feat_info", "mask_info"] # trafo_feat_info
  #which_feat_info = ["sum_feat_0", "sum_feat_1", "var_feat_0", "var_feat_1", "feat_info", "viewpair_mask"] # trafo_feat_info
  cond_thrustum_threshold = 0.0 # any of [0, 1, 2] used for threshold in query_cond_info 
  use_grouped_variance = False

  compress {
    first_compression_channels = 32
    out_channels = 16
    acivate_out = true
    method = "mean" # "weighted_sum" or "mean", "sum"

    cat_scales = false # Decides if we sum up the scales or concatenate them
    scales_method = "mean" # "sum" or "mean" --> Irrelevant if cat_scales is true
  }

  encoder {
    feature_channels = 128,
    num_scales = 1,
    num_head = 1,
    ffn_dim_expansion = 4,
    attn_splits_list = [2],
    cos_n_group = [2, 8],     #was 2,8
    # pretrain_weight = configs/pretrained_models/gmflow_sintel-0c07dcb3.pth,
    pretrain_weight = exp/dtu/matchsdf/checkpoints/ckpt_000000.pth
    num_transformer_layers = 6,
    feature_upsampler = network,
    upsample_factor = 2,
    use_multiview_gmflow = false,
    add_per_view_attn = false
    wo_self_attn = false
    feature_sample_local_radius = 0
    feature_sample_local_dilation = 1
  }

  sdf_network {
    ch_in = 3,
    hidden_dim = 128,
    num_sdf_layers = 4,

    # position embedding
    multires = 6

    # Ray similarity transformer                 
    use_ray_transformer = true
    use_second_sdf_network = true #if use_ray_transformer is false then second network won't be used anyway
    ray_transformer_heads = 4
    ray_transformer_d_model = 16
    ray_transformer_dim_feedforward = 32
    ray_transformer_use_full_layer = true

    skip_in = [4, 8, 12, 16, 20]
    switch_grad = 0     #switching grad with ray similarity transformer is problematic due to diff sampled_feat output shape
  }

  variance_network {
    init_val = 0.2
  }

  rendering_network {
    in_geometry_feat_ch = 10 #(this is sum of cos_n_group, multiplied by 2 if grouped variance is used as well) # default 8, then put as 16
    in_rendering_feat_ch = 0  #was 56
    anti_alias_pooling = True

  }


  trainer {
    n_samples = 64    #was 48, then 40
    n_importance = 64
    n_outside = 0  # 128 if render_outside_uniform_sampling
    perturb = 1 #was 1
    alpha_type = div
  }
}