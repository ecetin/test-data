"""
The codes are heavily borrowed from NeuS
"""

from copy import deepcopy
import os
import cv2 as cv
from misc.camera import get_coord_ref_ndc
from .gmflow.utils import sample_features_by_grid
from ops.grid_sampler import grid_sample_2d
from .render_utils import sample_pdf
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import logging
import mcubes
import trimesh
from icecream import ic

from scipy.spatial.transform import Rotation
from tqdm import tqdm
from pyhocon import ConfigFactory, ConfigTree

# SparseNeuS model libraries
from models.fields import SingleVarianceNetwork
# from models.trainer_generic import GenericTrainer
from models.sparse_sdf_network import SparseSdfNetwork
from models.rendering_network import GeneralRenderingNetwork

from .projector import Projector
from .rays import gen_single_ray_from_single_pose, compute_rotation_matrix
# from tsparse.torchsparse_utils import sparse_to_dense_channel

from typing import List, Tuple, Union, Optional, Dict, Any, Callable
from .fast_renderer import FastRenderer


class SparseNeuSRenderer(nn.Module):
    """
    conditional neus render;
    optimize on normalized world space;
    warped by nn.Module to support DataParallel traning
    """

    def __init__(self,
                 sdf_network: SparseSdfNetwork,
                 variance_network: SingleVarianceNetwork,
                 rendering_network: GeneralRenderingNetwork,
                 compress_layer: nn.Sequential,
                 n_samples: int,
                 n_importance: int,
                 n_outside: int,
                 perturb: float,
                 alpha_type: str = 'div',
                 conf: Optional[ConfigTree] = None
                 ):
        super(SparseNeuSRenderer, self).__init__()

        self.conf: ConfigTree = conf
        self.base_exp_dir: str = conf['general.base_exp_dir']

        # network setups
        self.sdf_network = sdf_network
        self.variance_network = variance_network
        self.rendering_network = rendering_network
        self.compress_layer = compress_layer

        self.n_samples = n_samples
        self.n_importance = n_importance
        self.n_outside = n_outside
        self.perturb = perturb
        self.alpha_type = alpha_type

        self.img_wh = self.conf.get_list('general.img_wh', default=[640, 512])

        self.rendering_projector = Projector()  # used to obtain features for generalized rendering

        self.h_patch_size: int = self.conf.get_int('model.h_patch_size', default=3)
        # self.patch_projector = PatchProjector(self.h_patch_size)

        self.ray_tracer = FastRenderer()  # ray_tracer to extract depth maps from sdf_volume

        self.which_feat_info: List[str] = self.conf.get_list('model.which_feat_info', default=['feat_info', 'color_info', 'mask_info'])
        
        self.use_sum_and_var: bool = 0 < sum([1 if ('sum_feat_0' in which_feat and 'sum_feat_1' in which_feat
                                                   and 'var_feat_0' in which_feat and 'var_feat_1') else 0 for which_feat in self.which_feat_info])      #NOTE:needs to check sum_feat_0, var_feat_0, sum_feat_1, var_feat_1. Currently all need to be specified to be used.
        self.use_new_feats: bool = 0 < sum([1 if ('new_feat' in which_feat) else 0 for which_feat in self.which_feat_info])
        
        self.cond_thrustum_threshold: float = self.conf.get_float('model.cond_thrustum_threshold', default=0.0)

        method_str = self.conf.get_string('model.compress.method', default='sum')

        # method for aggregating new feats
        self.weighted: bool = False
        if method_str == 'sum':
            self.method: Callable = torch.sum
        elif method_str == 'mean':
            self.method: Callable = torch.mean
        elif method_str == 'weighted_sum':
            self.method: Callable = torch.sum
            self.weighted = True
        elif method_str == 'weighted_mean':
            self.method: Callable = torch.mean
            self.weighted = True
        else:
            raise NotImplementedError(f'Unknown method {method_str}')
        

        # method for aggregating the scales of the new feats
        self.cat_scales: bool = self.conf.get_bool('model.compress.cat_scales', default=False) # Cat or add/mean different scales
        scales_method_str = self.conf.get_string('model.compress.scales_method', default='sum')
        self.scales_method: Callable = torch.sum if scales_method_str == 'sum' else torch.mean

        # sampling strategy for mesh generation
        self.sampling_strategy: str = self.conf.get_string('train.sampling_strategy', default='cubes')
        
        # Random sampling to maximize sdf
        self.rand_n_rays: int = self.conf.get_int('train.rand_n_rays', default=2)
        self.rand_n_points: int = self.conf.get_int('train.rand_n_points', default=512)
        self.rand_ray_x_points: int = int(self.rand_n_points*self.rand_n_rays)

        self.grid_sample_padding_mode: str = self.conf.get_string('train.grid_sample_padding_mode', default='border')

    def up_sample(self, 
                  rays_o: torch.Tensor, 
                  rays_d: torch.Tensor, 
                  z_vals: torch.Tensor, 
                  sdf: torch.Tensor, 
                  n_importance: int, 
                  inv_variance: int,
                  ref_poses: Dict[str, torch.Tensor],
                  imgs: torch.Tensor,
                  ref_feats_list: List[torch.Tensor],
                  ) -> torch.Tensor:
        """ Sampling to get better z_vals

        Args:
            rays_o (torch.Tensor): Ray origin
            rays_d (torch.Tensor): Ray direction
            z_vals (torch.Tensor): Z_vals
            sdf (torch.Tensor): Sdf outputs
            n_importance (int): number of importance samples
            inv_variance (int): inverse variance or so (no idea)

        Returns:
            torch.Tensor: z_vals
        """
        device = rays_o.device
        batch_size, n_samples = z_vals.shape
        pts = rays_o[:, None, :] + rays_d[:, None, :] * z_vals[..., :, None]  # n_rays, n_samples, 3

        cond_info = self.query_cond_info(pts[None,...], ref_poses, imgs, ref_feats_list, device)    #imgs are imgs supposedly
        pts_mask = (cond_info['mask_info'].sum(-1) > cond_info['threshold']).squeeze(0).type(torch.float32) 
        pts_mask = pts_mask[:, :-1] * pts_mask[:, 1:]

        sdf = sdf.reshape(batch_size, n_samples)
        prev_sdf, next_sdf = sdf[:, :-1], sdf[:, 1:]
        prev_z_vals, next_z_vals = z_vals[:, :-1], z_vals[:, 1:]
        mid_sdf = (prev_sdf + next_sdf) * 0.5
        
        dot_val = None
        if self.alpha_type == 'uniform':
            dot_val = torch.ones([batch_size, n_samples - 1]) * -1.0
        else:
            dot_val = (next_sdf - prev_sdf) / (next_z_vals - prev_z_vals + 1e-5)
            prev_dot_val = torch.cat([torch.zeros([batch_size, 1]).to(device), dot_val[:, :-1]], dim=-1)
            dot_val = torch.stack([prev_dot_val, dot_val], dim=-1)
            dot_val, _ = torch.min(dot_val, dim=-1, keepdim=False)
            dot_val = dot_val.clip(-10.0, 0.0) * pts_mask

        dist = (next_z_vals - prev_z_vals)
        prev_esti_sdf = mid_sdf - dot_val * dist * 0.5
        next_esti_sdf = mid_sdf + dot_val * dist * 0.5
        prev_cdf = torch.sigmoid(prev_esti_sdf * inv_variance)
        next_cdf = torch.sigmoid(next_esti_sdf * inv_variance)
        alpha_sdf = (prev_cdf - next_cdf + 1e-5) / (prev_cdf + 1e-5)

        alpha = alpha_sdf

        # - apply pts_mask
        alpha = pts_mask * alpha

        weights = alpha * torch.cumprod(
            torch.cat([torch.ones([batch_size, 1]).to(device), 1. - alpha + 1e-7], -1), -1)[:, :-1]

        z_samples = sample_pdf(z_vals, weights, n_importance, det=True).detach()
        return z_samples

    def cat_z_vals(self, 
                   rays_o: torch.Tensor, 
                   rays_d: torch.Tensor, 
                   z_vals: torch.Tensor, 
                   new_z_vals: torch.Tensor, 
                   sdf: torch.Tensor,
                   sdf_network: SparseSdfNetwork, 
                   ref_poses: Dict[str, torch.Tensor],
                   imgs: torch.Tensor,
                   ref_feats_list: List[torch.Tensor],
                   iter_step: int = 0
                   ) -> Tuple[torch.Tensor, torch.Tensor]:
        """ Sample new z_vals from the sdf volume

        Args:
            rays_o (torch.Tensor): Rays Origin
            rays_d (torch.Tensor): Rays Direction
            z_vals (torch.Tensor): Current z_vals
            new_z_vals (torch.Tensor): new z_vals
            sdf (torch.Tensor): sdf output
            sdf_network (SparseSdfNetwork): sdf network
            ref_poses (torch.Tensor): Reference poses
            imgs (torch.Tensor): Reference images
            ref_feats_list (List[torch.Tensor]): reference features

        Returns:
            torch.Tensor: catted z vals
        """
        device = rays_o.device
        batch_size, n_samples = z_vals.shape
        _, n_importance = new_z_vals.shape
        pts = rays_o[:, None, :] + rays_d[:, None, :] * new_z_vals[..., :, None]

        # _pts = deepcopy(pts)
        # _pts = _pts[None,...]

        cond_info = self.query_cond_info(pts.unsqueeze(0), ref_poses, imgs, ref_feats_list, device)    #imgs are imgs supposedly
        cond_feats = self.query_cond_feats(cond_info)

        pts_mask_bool = (cond_info['mask_info'].sum(-1) > cond_info['threshold']).view(-1)
        pts_mask = pts_mask_bool.type(torch.float32)

        new_sdf = torch.ones([batch_size * n_importance, 1]).to(pts.dtype).to(device) * 100

        # If we use ray similarity transformer, we should update the whole sdf volume if any of the points is inside frustum mask
        if sdf_network.use_ray_transformer:
            if torch.sum(pts_mask) > 1:
                new_outputs = sdf_network.sdf(pts.reshape(-1,3), cond_feats, iter_step=iter_step)
                new_sdf = new_outputs['sdf_pts']  # .reshape(batch_size, n_importance)
        else:
            pts_mask = pts_mask_bool.type(torch.float32)

            pts_ = pts.reshape(-1, 3)
            sampled_feats = cond_feats.view(pts_.shape[0], -1)

            if torch.sum(pts_mask) > 1:
                new_outputs = sdf_network.sdf(pts_[pts_mask_bool], sampled_feats[pts_mask_bool])
                new_sdf[pts_mask_bool] = new_outputs['sdf_pts']  # .reshape(batch_size, n_importance)

        new_sdf = new_sdf.view(batch_size, n_importance)

        z_vals = torch.cat([z_vals, new_z_vals], dim=-1)
        sdf = torch.cat([sdf, new_sdf], dim=-1)

        z_vals, index = torch.sort(z_vals, dim=-1)
        xx = torch.arange(batch_size)[:, None].expand(batch_size, n_samples + n_importance).reshape(-1)
        index = index.reshape(-1)
        sdf = sdf[(xx, index)].reshape(batch_size, n_samples + n_importance)

        return z_vals, sdf

    def render_core(self,
                    rays_o: torch.Tensor,
                    rays_d: torch.Tensor,
                    z_vals: torch.Tensor,
                    ref_poses: Dict[str, torch.Tensor],
                    ref_feats_list: List[torch.Tensor],
                    tgt_pose: Dict[str, torch.Tensor],
                    sample_dist: torch.NumberType,
                    sdf_network: SparseSdfNetwork,
                    rendering_network: GeneralRenderingNetwork,
                    feature_maps: torch.Tensor,
                    imgs: torch.Tensor,
                    background_rgb: Optional[torch.Tensor] = None,  # - no use here
                    alpha_inter_ratio: float = 0.0,
                    img_wh: List[int] = [640, 360],
                    query_c2w: Optional[torch.Tensor] = None,  # - used for testing
                    if_general_rendering: bool = True,
                    if_render_with_grad: bool = True,
                    iter_step: int = 0,
                    # * used for clear bg and fg
                    bg_num: int = 0
                    ) -> Dict[str, torch.Tensor]:
        """ Core function for rendering
        
        Args:
            rays_o (torch.Tensor): [N_rays, 3]
            rays_d (torch.Tensor): [N_rays, 3]
            z_vals (torch.Tensor): [N_rays, N_samples]
            ref_poses (Dict[str, torch.Tensor]): [N_refs, 4, 4]
            ref_feats_list (List[torch.Tensor]): [N_refs, C, H, W]
            tgt_pose (Dict[str, torch.Tensor]): [4, 4]
            sample_dist (torch.NumberType): [1]
            sdf_network (SparseSdfNetwork): sdf network
            rendering_network (GeneralRenderingNetwork): rendering network
            feature_maps (torch.Tensor): [N_refs, C, H, W]
            imgs (torch.Tensor): [N_refs, 3, H, W]
            background_rgb (Optional[torch.Tensor], optional): [3]. Defaults to None.
            alpha_inter_ratio (float, optional): [0, 1]. Defaults to 0.0.
            img_wh (List[int], optional): [2]. Defaults to [640, 360].
            query_c2w (Optional[torch.Tensor], optional): [4, 4]. Defaults to None.
            if_general_rendering (bool, optional): [description]. Defaults to True.
            if_render_with_grad (bool, optional): [description]. Defaults to True.
            bg_num (int, optional): [description]. Defaults to 0.
            
        Returns:
            Dict[str, torch.Tensor]: Output dict with everything we need
        """
        device = rays_o.device
        N_rays = rays_o.shape[0]
        _, n_samples = z_vals.shape
        dists = z_vals[..., 1:] - z_vals[..., :-1]
        dists = torch.cat([dists, torch.Tensor([sample_dist]).expand(dists[..., :1].shape).to(device)], -1)

        mid_z_vals = z_vals + dists * 0.5
        mid_dists = mid_z_vals[..., 1:] - mid_z_vals[..., :-1]

        pts = rays_o[:, None, :] + rays_d[:, None, :] * mid_z_vals[..., :, None]  # n_rays, n_samples, 3
        dirs = rays_d[:, None, :].expand(pts.shape)
        
        cond_info = self.query_cond_info(pts[None,...], ref_poses, imgs, ref_feats_list, device)
        cond_feats = self.query_cond_feats(cond_info)        

        pts_mask_bool = (cond_info['mask_info'][0].sum(-1) > cond_info['threshold']) 
        grad_pts = pts.clone()
        dirs = dirs.reshape(-1, 3)

        # If we use ray similarity transformer, we should update the whole sdf volume if any of the points is inside frustum mask
        if sdf_network.use_ray_transformer:
            # If all points are outside frustum mask, we still take a single ray to update sdf volume
            if torch.sum(pts_mask_bool) < 1:
                pts_mask_bool[0] = True
            
            # check if any rays are completely outside and don't update these but update all other rays completely
            pts_mask_bool = (torch.sum(pts_mask_bool, dim=1) > 1).reshape(-1,1).repeat(1, n_samples) # (n_rays) 
            sampled_feats = cond_feats[pts_mask_bool[:,0].unsqueeze(0)] # N_rays_sampled * n_samples, C_latent
            sdf_nn_output = sdf_network.sdf(pts[pts_mask_bool], sampled_feats.unsqueeze(0), iter_step=iter_step)
            cond_feats = sampled_feats.unsqueeze(0)
        else:
            if torch.sum(pts_mask_bool.float()) < 1:  # ! when render out image, may meet this problem
                pts_mask_bool[:100] = True
            
            sdf_nn_output = sdf_network.sdf(pts[pts_mask_bool], cond_feats[pts_mask_bool.unsqueeze(0)])
            cond_feats = cond_feats[pts_mask_bool.unsqueeze(0)]

        pts_mask = pts_mask_bool.type(torch.float32)
        sdf = torch.ones([N_rays * n_samples, 1]).to(pts.dtype).to(device) * 100
        sdf[pts_mask_bool.view(-1)] = sdf_nn_output['sdf_pts']  # [N_rays*n_samples, 1]
        feature_vector_valid = sdf_nn_output['sdf_features_pts']
        feature_vector = torch.zeros([N_rays * n_samples, feature_vector_valid.shape[1]]).to(pts.dtype).to(device)
        feature_vector[pts_mask_bool.view(-1)] = feature_vector_valid

        # * estimate alpha from sdf
        if not sdf_network.use_ray_transformer:
            gradients = torch.zeros([N_rays * n_samples, 3]).to(pts.dtype).to(device)
            pts_grads, feature_grads = sdf_network.gradient(
                grad_pts, ref_poses, ref_feats_list, imgs, self.query_cond_info, self.query_cond_feats, iter_step
            )
            
            gradients[pts_mask_bool.view(-1)] = pts_grads

        sampled_color_mlp = None
        rendering_valid_mask_mlp = None
        sampled_color_patch = None
        rendering_patch_mask = None

        if if_general_rendering:  # used for general training
            ren_ray_diff = self.rendering_projector.compute(
                pts.view(N_rays, n_samples, 3),
                # * 2d rendering feature maps
                rendering_feature_maps=feature_maps,
                color_maps=imgs,
                w2cs=ref_poses["extrinsics"],
                intrinsics=ref_poses["intrinsics"],
                img_wh=img_wh,
                query_img_idx=-1,  # the index of the N_views dim for rendering
                query_c2w=tgt_pose,
            )
            _pts = pts.reshape(1, N_rays, n_samples, 3)

            cond_info = self.query_cond_info(_pts, ref_poses, imgs, ref_feats_list, device)    #imgs are imgs supposedly
            ren_geo_feats = cond_info['feat_info'][0]
            ren_rgb_feats = cond_info['color_info']
            ren_mask = cond_info['mask_info'][0]
            ren_mask = ren_mask.permute(2,0,1)  
            
            ren_rgb_feats = torch.cat(torch.split(ren_rgb_feats,3,dim=-1),dim=0)        #NOTE:[1:] because query_img_indx is 0


            if if_render_with_grad:
                sampled_color, rendering_valid_mask = rendering_network(
                    ren_geo_feats, ren_rgb_feats, ren_ray_diff, ren_mask)
            else:
                with torch.no_grad():
                    sampled_color, rendering_valid_mask = rendering_network(
                        ren_geo_feats, ren_rgb_feats, ren_ray_diff, ren_mask)
        else:
            sampled_color, rendering_valid_mask = None, None

        inv_variance = self.variance_network(feature_vector)[:, :1].clip(1e-6, 1e6)

        if not sdf_network.use_ray_transformer:
            true_dot_val = (dirs * gradients).sum(-1, keepdim=True)  # * calculate
            iter_cos = -(F.relu(-true_dot_val * 0.5 + 0.5) * (1.0 - alpha_inter_ratio) + F.relu(
                -true_dot_val) * alpha_inter_ratio)  # always non-positive
            
            iter_cos = iter_cos * pts_mask.view(-1, 1)
        
        else:
            # Taken from VolRecon
            true_dot_val = -1.0
            iter_cos = -(-true_dot_val * 0.5 + 0.5 * (1.0 - alpha_inter_ratio) -true_dot_val * alpha_inter_ratio)  # always non-positive

        true_estimate_sdf_half_next = sdf + iter_cos.clip(-10.0, 10.0) * dists.reshape(-1, 1) * 0.5
        true_estimate_sdf_half_prev = sdf - iter_cos.clip(-10.0, 10.0) * dists.reshape(-1, 1) * 0.5

        prev_cdf = torch.sigmoid(true_estimate_sdf_half_prev * inv_variance)
        next_cdf = torch.sigmoid(true_estimate_sdf_half_next * inv_variance)

        p = prev_cdf - next_cdf
        c = prev_cdf

        # NOTE: We are using 'div'!
        if self.alpha_type == 'div':
            alpha_sdf = ((p + 1e-5) / (c + 1e-5)).reshape(N_rays, n_samples).clip(0.0, 1.0)
        elif self.alpha_type == 'uniform':
            uniform_estimate_sdf_half_next = sdf - dists.reshape(-1, 1) * 0.5
            uniform_estimate_sdf_half_prev = sdf + dists.reshape(-1, 1) * 0.5
            uniform_prev_cdf = torch.sigmoid(uniform_estimate_sdf_half_prev * inv_variance)
            uniform_next_cdf = torch.sigmoid(uniform_estimate_sdf_half_next * inv_variance)
            uniform_alpha = F.relu(
                (uniform_prev_cdf - uniform_next_cdf + 1e-5) / (uniform_prev_cdf + 1e-5)).reshape(
                N_rays, n_samples).clip(0.0, 1.0)
            alpha_sdf = uniform_alpha
        else:
            assert False

        alpha = alpha_sdf

        # - apply pts_mask
        alpha = alpha * pts_mask

        # pts_radius = torch.linalg.norm(pts, ord=2, dim=-1, keepdim=True).reshape(N_rays, n_samples)
        # inside_sphere = (pts_radius < 1.0).float().detach()
        # relax_inside_sphere = (pts_radius < 1.2).float().detach()
        inside_sphere = pts_mask
        # relax_inside_sphere = pts_mask

        weights = alpha * torch.cumprod(torch.cat([torch.ones([N_rays, 1]).to(device), 1. - alpha + 1e-7], -1), -1)[:,
                          :-1]  # n_rays, n_samples
        weights_sum = weights.sum(dim=-1, keepdim=True)
        alpha_sum = alpha.sum(dim=-1, keepdim=True)

        if bg_num > 0:
            weights_sum_fg = weights[:, :-bg_num].sum(dim=-1, keepdim=True)
        else:
            weights_sum_fg = weights_sum

        if sampled_color is not None:
            color = (sampled_color * weights[:, :, None]).sum(dim=1)
        else:
            color = None

        if background_rgb is not None and color is not None:
            color = color + background_rgb * (1.0 - weights_sum)

        ###################*  mlp color rendering  #####################
        color_mlp = None
        if sampled_color_mlp is not None:
            color_mlp = (sampled_color_mlp * weights[:, :, None]).sum(dim=1)

        if background_rgb is not None and color_mlp is not None:
            color_mlp = color_mlp + background_rgb * (1.0 - weights_sum)

        ############################ *  patch blending  ################
        blended_color_patch = None
        if sampled_color_patch is not None:
            blended_color_patch = (sampled_color_patch * weights[:, :, None, None]).sum(dim=1)  # [N_rays, Npx, 3]

        ######################################################

        if not sdf_network.use_ray_transformer:
            gradient_error = (torch.linalg.norm(gradients.reshape(N_rays, n_samples, 3), ord=2,
                                                dim=-1) - 1.0) ** 2
            # ! the gradient normal should be masked out, the pts out of the bounding box should also be penalized
            gradient_error = (pts_mask * gradient_error).sum() / (
                    (pts_mask).sum() + 1e-5)
        else:
            gradient_error = torch.ones((N_rays, n_samples, 3)).to(sdf.device) * (-1.)

        depth = (mid_z_vals * weights[:, :n_samples]).sum(dim=1, keepdim=True)

        return {
            'color': color,
            'color_mask': rendering_valid_mask,  # (N_rays, 1)
            'color_mlp': color_mlp,
            'color_mlp_mask': rendering_valid_mask_mlp,
            'sdf': sdf,  # (N_rays, n_samples)
            'depth': depth,  # (N_rays, 1)
            'dists': dists,
            'gradients': gradients.reshape(N_rays, n_samples, 3),
            'variance': 1.0 / inv_variance,
            'mid_z_vals': mid_z_vals,
            'weights': weights,
            'weights_sum': weights_sum,
            'alpha_sum': alpha_sum,
            'alpha_mean': alpha.mean(),
            'cdf': c.reshape(N_rays, n_samples),
            'gradient_error': gradient_error,
            'inside_sphere': inside_sphere,
            'blended_color_patch': blended_color_patch,
            'blended_color_patch_mask': rendering_patch_mask,
            'weights_sum_fg': weights_sum_fg
        }

    def render(self, 
               rays_o: torch.Tensor, 
               rays_d: torch.Tensor, 
               near: torch.Tensor, 
               far: torch.Tensor, 
               sdf_network: SparseSdfNetwork, 
               rendering_network: GeneralRenderingNetwork,
               ref_feats_list: List[torch.Tensor],
               imgs: torch.Tensor,
               ref_poses: Dict[str, torch.Tensor], 
               tgt_pose: Dict[str, torch.Tensor],
               background_rgb: Optional[torch.Tensor] = None,
               alpha_inter_ratio: float = 0.0,
               # * 2d feature maps
               img_wh: List[int] = [640, 360],
               query_c2w: Optional[torch.Tensor] = None,  # -used for testing
               if_general_rendering: bool = True,
               if_render_with_grad: bool = True,
               iter_step: int = 0,
               # * for clear foreground
               bg_ratio: bool = 0.0,
               perturb_overwrite: int = -1
               ) -> Dict[str, torch.Tensor]:
        """ _render

        Args:
            rays_o (torch.Tensor): Rays origin. (N_rays, 3)
            rays_d (torch.Tensor): Rays direction. (N_rays, 3)
            near (torch.Tensor): Near bound. (N_rays, 1)
            far (torch.Tensor): Far bound. (N_rays, 1)
            sdf_network (SparseSdfNetwork): SDF network.
            rendering_network (GeneralRenderingNetwork): General rendering network.
            ref_feats_list (List[torch.Tensor]): Reference feature maps from the encoder.
            imgs (torch.Tensor): Reference Images
            ref_poses (Dict[str, torch.Tensor]): Reference poses.
            tgt_pose (Dict[str, torch.Tensor]): target pose.
            background_rgb (Optional[torch.Tensor], optional): Background RGB color. Defaults to None.
            alpha_inter_ratio (float, optional): _description_. Defaults to 0.0.
            img_wh (List[int], optional): Image width and height. Defaults to [640, 360].
            query_c2w (Optional[torch.Tensor], optional): Target Cam2world. Defaults to None.
            if_render_with_grad (bool, optional): _description_. Defaults to True.
            bg_ratio (bool, optional): _description_. Defaults to 0.0.
            perturb_overwrite (int, optional): _description_. Defaults to -1.

        Returns:
            Dict[str, torch.Tensor]: Output
        """
        device = rays_o.device
        N_rays = len(rays_o)
        
        # distance between each sample on each ray 
        sample_dist = ((far - near) / self.n_samples).mean().item()

        # Relative distance between each sample on each ray
        z_vals = torch.linspace(0.0, 1.0, self.n_samples).to(device)
        z_vals = near + (far - near) * z_vals[None, :] # scale to the range of [near, far]

        bg_num = int(self.n_samples * bg_ratio)

        if z_vals.shape[0] == 1:
            z_vals = z_vals.repeat(N_rays, 1)

        if bg_num > 0:
            z_vals_bg = z_vals[:, self.n_samples - bg_num:]
            z_vals = z_vals[:, :self.n_samples - bg_num]

        n_samples = self.n_samples - bg_num
        perturb = self.perturb

        if perturb_overwrite >= 0:
            perturb = perturb_overwrite
        if perturb > 0:
            # get intervals between samples
            mids = .5 * (z_vals[..., 1:] + z_vals[..., :-1])
            upper = torch.cat([mids, z_vals[..., -1:]], -1)
            lower = torch.cat([z_vals[..., :1], mids], -1)
            # stratified samples in those intervals
            t_rand = torch.rand(z_vals.shape).to(device)
            z_vals = lower + (upper - lower) * t_rand

        ###########################################################################################################
        # NOTE: Removing up-sampling for now!
        ###########################################################################################################
        # Up sample
        if self.n_importance > 0:
            with torch.no_grad():
                pts = rays_o[:, None, :] + rays_d[:, None, :] * z_vals[..., :, None]

                cond_info = self.query_cond_info(pts[None,...], ref_poses, imgs, ref_feats_list, device)  
                cond_feats = self.query_cond_feats(cond_info)                
                
                # print("cond_feats shape:", cond_feats.shape)
                sdf_outputs = sdf_network.sdf(pts.reshape(-1, 3), cond_feats, iter_step=iter_step)   

                # pdb.set_trace()
                sdf = sdf_outputs['sdf_pts'].reshape(N_rays, self.n_samples - bg_num)

                n_steps = 4        #was 4
                for i in range(n_steps):
                    new_z_vals = self.up_sample(rays_o, rays_d, z_vals, sdf, self.n_importance // n_steps,
                                                64 * 2 ** i, ref_poses=ref_poses, imgs=imgs, ref_feats_list=ref_feats_list
                                                # conditional_valid_mask_volume=None    #NOTE:mask was conditional_valid_mask_volume
                                                )
                    
                    z_vals, sdf = self.cat_z_vals(
                        rays_o, rays_d, z_vals, new_z_vals, sdf,
                        sdf_network,
                        ref_feats_list=ref_feats_list,
                        ref_poses=ref_poses,
                        imgs=imgs,
                        iter_step=iter_step
                    )
                del sdf

            n_samples = self.n_samples + self.n_importance

        ###########################################################################################################
        ###########################################################################################################

        # Background
        ret_outside = None

        # Render
        if bg_num > 0:
            z_vals = torch.cat([z_vals, z_vals_bg], dim=1)
        ret_fine = self.render_core(rays_o=rays_o,
                                    rays_d=rays_d,
                                    z_vals=z_vals,
                                    ref_poses=ref_poses,
                                    ref_feats_list=ref_feats_list,
                                    tgt_pose=tgt_pose,
                                    sample_dist=sample_dist,
                                    sdf_network=sdf_network,
                                    rendering_network=rendering_network,
                                    feature_maps=cond_feats,              
                                    imgs=imgs,
                                    background_rgb=background_rgb,
                                    alpha_inter_ratio=alpha_inter_ratio,
                                    img_wh=img_wh,
                                    query_c2w=query_c2w,
                                    if_general_rendering=if_general_rendering,
                                    if_render_with_grad=if_render_with_grad,
                                    iter_step=iter_step
                                    )

        color_fine = ret_fine['color']

        if self.n_outside > 0:
            color_fine_mask = torch.logical_or(ret_fine['color_mask'], ret_outside['color_mask'])
        else:
            color_fine_mask = ret_fine['color_mask']

        weights = ret_fine['weights']
        weights_sum = ret_fine['weights_sum']

        gradients = ret_fine['gradients']
        mid_z_vals = ret_fine['mid_z_vals']

        # depth = (mid_z_vals * weights[:, :n_samples]).sum(dim=1, keepdim=True)
        depth = ret_fine['depth']
        depth_varaince = ((mid_z_vals - depth) ** 2 * weights[:, :n_samples]).sum(dim=-1, keepdim=True)
        variance = ret_fine['variance'].reshape(N_rays, n_samples).mean(dim=-1, keepdim=True)

        # - randomly sample points from the volume, and maximize the sdf
        if self.sampling_strategy == 'cubes':
            # Generate a torch.tensor of shape (1, 1, rand_n_points*rand_n_rays, 3) as points that are 
            #       uniformly sampled from a cube of side length 2 centered at the origin
            pts_random = torch.rand([1, 1, self.rand_ray_x_points, 3]).float().to(device) * 2 - 1  # normalized to (-1, 1)
        elif self.sampling_strategy == 'rays':
            # Generate a torch.tensor of shape (1, rand_n_rays, rand_n_points, 3) as rays that are cats forwards 
            #       in a random direction between (-1, -1, -1) and (1,1,1)
            pts_random = self.generate_random_rays(device=device,
                                                   n_rays=self.rand_n_rays, 
                                                   n_points=self.rand_n_points) 

        cond_info = self.query_cond_info(
            pts_random, ref_poses, imgs, ref_feats_list, device
        )    #imgs are imgs supposedly
        cond_feats = self.query_cond_feats(cond_info)

        sdf_random = sdf_network.sdf(pts_random.reshape(-1, 3), cond_feats, iter_step=iter_step)['sdf_pts']  #cond_feats was conditional_volume
        

        result = {
            'depth': depth,
            'color_fine': color_fine,
            'color_fine_mask': color_fine_mask,
            'color_outside': ret_outside['color'] if ret_outside is not None else None,
            'color_outside_mask': ret_outside['color_mask'] if ret_outside is not None else None,
            'color_mlp': ret_fine['color_mlp'],
            'color_mlp_mask': ret_fine['color_mlp_mask'],
            'variance': variance.mean(),
            'cdf_fine': ret_fine['cdf'],
            'depth_variance': depth_varaince,
            'weights_sum': weights_sum,
            'weights_max': torch.max(weights, dim=-1, keepdim=True)[0],
            'alpha_sum': ret_fine['alpha_sum'].mean(),
            'alpha_mean': ret_fine['alpha_mean'],
            'gradients': gradients,
            'weights': weights,
            'gradient_error_fine': ret_fine['gradient_error'],
            'inside_sphere': ret_fine['inside_sphere'],
            'sdf': ret_fine['sdf'],
            'sdf_random': sdf_random,
            'blended_color_patch': ret_fine['blended_color_patch'],
            'blended_color_patch_mask': ret_fine['blended_color_patch_mask'],
            'weights_sum_fg': ret_fine['weights_sum_fg'],
            'cond_feat': cond_feats,
        }

        return result

    def generate_random_rays(self, 
                             device: torch.device,
                             n_rays: int = 4,
                             n_points: int = 128
                             ) -> torch.Tensor:

        # Generate random Euler angles for each ray
        euler_angles = torch.randn(n_rays, 3) * np.pi/2  # Random Euler angles with shape (n_rays, 3)

        # Convert Euler angles to rotation matrices
        rotation_matrices = []
        rotation_matrices = np.zeros((n_rays, 3, 3))
        for i in range(n_rays):
            r = Rotation.from_euler('xyz', euler_angles[i], degrees=False)
            rotation_matrices[i] = r.as_matrix()

        rotation_matrices = torch.tensor(rotation_matrices)  # Shape: (n_rays, 3, 3)

        # Generate a linspace vector between -1 and 1
        linspace = torch.linspace(-1, 1, n_points).unsqueeze(0).repeat(3,1).double()  # Shape: (3, n_points)

        # Rotate the linspace vector with the rotation matrices for each ray
        rays = torch.matmul(linspace.T.unsqueeze(1).unsqueeze(1), rotation_matrices).squeeze(2).unsqueeze(0) # Shape: (1, n_points, n_rays, 3)
        return rays.float().permute(0, 2, 1, 3).to(device) # Shape: (1, n_rays, n_points, 3)


    def query_cond_info(self, 
                        point_samples: torch.Tensor, 
                        ref_poses: Dict[str, torch.Tensor], 
                        ref_images: torch.Tensor, 
                        ref_feats_list: List[torch.Tensor], 
                        device: torch.device
                        ) -> Dict[str, torch.Tensor]:
        """Query the conditional information from the reference images and poses using the World Pose as target. 
           Adapted from MatchNeRF

        Args:
            point_samples (torch.Tensor): 3D Points to query the conditional information. B, n_rays, n_samples, 3
            ref_poses (Dict[str, torch.Tensor]): Reference poses: all camera information of reference images 
                'extrinsics': B, N, 3, 4; 'intrinsics': B, N, 3, 3; 'near_fars': B, N, 2
            ref_images (torch.Tensor): Reference images: B, n_views, 3, H, W. range: [0, 1] !!!
            ref_feats_list (List[torch.Tensor]): Reference image features List
            device (torch.device): Device to use

        Returns:
            Dict[str, torch.Tensor]: Conditional Information
        """
        batch_size, n_views, _, img_h, img_w = ref_images.shape

        assert ref_feats_list is not None, "Must provide the image feature for info query."
        cos_n_group = self.conf['model.encoder.cos_n_group']
        use_grouped_variance = self.conf['model.use_grouped_variance']
        cos_n_group = [cos_n_group] if isinstance(cos_n_group, int) else cos_n_group
        feat_data_list = [[] for _ in range(len(ref_feats_list))]
        color_data = []
        mask_data = []

        # query information from each source view
        inv_scale = torch.tensor([[img_w - 1, img_h - 1]]).repeat(batch_size, 1).to(device)
        for view_idx in range(n_views):
            near_far_ref = ref_poses['near_fars'][:, view_idx]

            extr_ref, intr_ref = ref_poses['extrinsics'][:, view_idx, :3].clone(), ref_poses['intrinsics'][:, view_idx].clone()
            point_samples_pixel = get_coord_ref_ndc(extr_ref, intr_ref, point_samples,
                                                    inv_scale, near_far=near_far_ref)
            grid = point_samples_pixel[..., :2] * 2.0 - 1.0

            # record visibility mask for further usage
            in_mask = ((grid > -1.0) * (grid < 1.0))
            in_mask = (in_mask[..., 0] * in_mask[..., 1]).float()
            unsqueezed_in_mask = in_mask.unsqueeze(1)
            mask_data.append(unsqueezed_in_mask)
            
            # query enhanced features infomation from each view
            for scale_idx, img_feat_cur_scale in enumerate(ref_feats_list):
                raw_whole_feats = img_feat_cur_scale[:, view_idx]
                sampled_feats = sample_features_by_grid(
                    raw_whole_feats, grid, align_corners=True, mode='bilinear', padding_mode='zeros',
                    local_radius=self.conf['model.encoder.feature_sample_local_radius'],
                    local_dilation=self.conf['model.encoder.feature_sample_local_dilation']
                )
                feat_data_list[scale_idx].append(sampled_feats)

            # query color
            color_data.append(grid_sample_2d(ref_images[:, view_idx], grid))
            # color_data.append(F.grid_sample(ref_images[:, view_idx], grid, align_corners=True, mode='bilinear', padding_mode='zeros'))

        merged_mask_data = torch.cat(mask_data, dim=1)

        # merge queried information from all views
        all_data = {}
        # merge extracted enhanced features
        merged_feat_data = []
        merged_viewpair_mask = []
        new_feats_list = []
        for feat_data_idx, raw_feat_data in enumerate(feat_data_list):  # loop over scale
            cur_updated_feat_data = []
            var_updated_feat_data = []
            total_mask = []
            # split back to original
            # breakpoint()
            split_feat_data = [torch.split(x, int(x.shape[1] / (n_views - 1)), dim=1) for x in raw_feat_data]
            # calculate simliarity for feature from the same transformer
            index_lists = [(a, b) for a in range(n_views - 1) for b in range(a, n_views - 1)]
            for i_idx, j_idx in index_lists:
                # breakpoint()
                input_a = split_feat_data[i_idx][j_idx]  # B x C x N_rays x N_pts
                input_b = split_feat_data[j_idx + 1][i_idx]

                if use_grouped_variance:
                    tmp = torch.stack([input_a, input_b],dim=1)
                    iB, iG, iC, iR, iP = tmp.shape
                    var_tmp = torch.var(tmp, dim=1)
                    #split var_tmp along dimension 1 into cos_n_group parts, then get the average of each part
                    var_feat = torch.mean(torch.stack(torch.split(var_tmp,  cos_n_group[feat_data_idx], dim=1),dim=1), dim=1)

                iB, iC, iR, iP = input_a.shape
                group_a = input_a.reshape(iB, cos_n_group[feat_data_idx], int(iC / cos_n_group[feat_data_idx]), iR, iP)
                group_b = input_b.reshape(iB, cos_n_group[feat_data_idx], int(iC / cos_n_group[feat_data_idx]), iR, iP)
                cos_sim = torch.nn.CosineSimilarity(dim=2)(group_a, group_b)

                # Get the mask for each frustum
                cos_sim_mask = torch.index_select(merged_mask_data, dim=1, index=torch.tensor([i_idx, j_idx+1]).cuda())
                cos_sim_mask_sum = torch.sum(cos_sim_mask, dim=1)
                tmp_frustum_sum = cos_sim_mask_sum > 1.0 # If both views see the point, then it's valid
                total_mask.append(tmp_frustum_sum.float())

                cur_updated_feat_data.append(cos_sim * tmp_frustum_sum.unsqueeze(1))

                if use_grouped_variance:
                    var_updated_feat_data.append(var_feat * tmp_frustum_sum.unsqueeze(1))
            
            cur_updated_feat_data = torch.stack(cur_updated_feat_data, dim=1)  # [B, n_pairs, n_groups, n_rays, n_pts]
            if use_grouped_variance:
                var_updated_feat_data = torch.stack(var_updated_feat_data, dim=1)  # [B, n_pairs, n_groups, n_rays, n_pts]

            total_mask_sum = torch.stack(total_mask, dim=1).sum(dim=1, keepdim=True)

            # Clamp the total mask sum to avoid nan
            total_mask_sum = torch.clamp(total_mask_sum, min=1.0)
            cur_updated_feat_data = torch.sum(cur_updated_feat_data, dim=1, keepdim=True) / total_mask_sum
            cur_updated_feat_data = cur_updated_feat_data.reshape(cur_updated_feat_data.shape[0], -1, *cur_updated_feat_data.shape[-2:])
            merged_feat_data.append(cur_updated_feat_data)

            if use_grouped_variance:
                var_updated_feat_data = torch.sum(var_updated_feat_data, dim=1, keepdim=True) / total_mask_sum
                var_updated_feat_data = var_updated_feat_data.reshape(var_updated_feat_data.shape[0], -1, *var_updated_feat_data.shape[-2:])
                merged_feat_data.append(var_updated_feat_data)

            merged_viewpair_mask.append(total_mask_sum / n_views) # Technically it's both times the same mask

            # self.method is either torch.sum or torch.mean
            if self.weighted:
                new_feat_tmp = self.method(torch.stack(raw_feat_data, dim=1) * merged_mask_data.unsqueeze(-3), dim=1)
            else:
                new_feat_tmp = self.method(torch.stack(raw_feat_data, dim=1), dim=1)
            new_feats_list.append(new_feat_tmp)

        if self.use_new_feats:
            new_feats = torch.permute(torch.stack(new_feats_list), (1, 3, 4, 0, 2))
            if not self.cat_scales:
                # aggregate the features from different scales using either sum or mean (defined in config), 
                #   then feed the aggregated features through the compress layer 
                new_feats = self.scales_method(new_feats, dim=-2)
                additional_feats_new = self.compress_layer(new_feats)
            else:
                # Feed the catted features through the compress layer and then flatten last 2 layers:
                #   B, X, Y, Scales, Feat-Size -> B, X, Y, Scales * Feat-Size
                additional_feats_new = self.compress_layer(new_feats).flatten(-2)
            
            # permute to match the shape of the other features
            additional_feats_new = additional_feats_new.permute((0, 3, 1, 2))
            all_data['new_feat'] = additional_feats_new
        
        if self.use_sum_and_var:
            #variance of projections on feature maps. Calculate variance on both scales
            var_feat = torch.stack([torch.var(torch.stack(list_i, dim=0), dim=0) for list_i in feat_data_list], dim=0)
            # var_feat = torch.sum(var_feat, dim=0) / len(feat_data_list)   # average variance on both scales
            # var_feat = torch.stack(var_feat,dim = 0)

            sum_feat =  torch.stack([torch.sum(torch.stack(list_i, dim=0), dim=0) for list_i in feat_data_list], dim=0)
            
            # new_feat = torch.sum(
            #     torch.stack([torch.sum(torch.stack(list_i, dim=0), dim=0) for list_i in feat_data_list], dim=0), 
            #     dim=0
            # ) / (len(feat_data_list) * len(feat_data_list[0]))

            N, C, R, P = sum_feat[0].shape
            # new_feat = torch.permute(new_feat, (0, 2, 3, 1))
            # additional_feats = self.compress_layer(new_feat.view(-1, new_feat.shape[-1]))   #NOTE: maybe we could use CNN for compress layer since points on ray have spatial context (in sparseNeuS they used 3DCNN for vertices)
            # additional_feats = torch.permute(additional_feats.reshape(N, R, P, -1), (0, 3, 1, 2))
            # all_data['new_feat'] = additional_feats

            for scale in range(len(feat_data_list)):
                var_feat_tmp = var_feat[scale]
                # breakpoint()
                var_feat_tmp = torch.permute(var_feat_tmp, (0, 2, 3, 1))
                additional_feats = self.compress_layer(var_feat_tmp.view(-1, var_feat_tmp.shape[-1]))
                additional_feats = torch.permute(additional_feats.reshape(N, R, P, -1), (0, 3, 1, 2))

                sum_feat_tmp = sum_feat[scale]
                sum_feat_tmp = torch.permute(sum_feat_tmp, (0, 2, 3, 1))
                additional_feats2 = self.compress_layer(sum_feat_tmp.view(-1, sum_feat_tmp.shape[-1]))  #not sure if better to use different compress layer
                additional_feats2 = torch.permute(additional_feats2.reshape(N, R, P, -1), (0, 3, 1, 2))

                all_data['var_feat_{}'.format(scale)] = additional_feats
                all_data['sum_feat_{}'.format(scale)] = additional_feats2

        merged_feat_data = torch.cat(merged_feat_data, dim=1)
        
        # all_data.append(merged_feat_data)
        all_data['feat_info'] = merged_feat_data

        # merge extracted color data
        merged_color_data = torch.cat(color_data, dim=1)
        # all_data.append(merged_color_data)
        all_data['color_info'] = merged_color_data

        # merge visibility masks
        # all_data.append(merged_mask_data)
        all_data['mask_info'] = merged_mask_data

        # valid supporting viewpairs
        # Take the mean over both scales, so that the gardient can be backpropagated to the transformer
        all_data['viewpair_mask'] = torch.cat(merged_viewpair_mask, dim=1).mean(dim=1, keepdim=True)

        # breakpoint()
        # all_data = torch.cat(all_data, dim=1)[0].permute(1, 2, 0)
        for k, v in all_data.items():
            all_data[k] = v.permute(0, 2, 3, 1)  # (b, n_rays, n_samples, n_dim)

        all_data['threshold'] = self.cond_thrustum_threshold
        return all_data

    def query_cond_feats(self, 
                         cond_info: Dict[str, torch.Tensor]
                         ) -> torch.Tensor:
        """ Concatenate the elements of cond_info according to self.which_feat_info by their last dimension

        Args:
            cond_info (Dict[str, torch.Tensor]): Conditional Info from query_cond_info

        Returns:
            torch.Tensor: Concatenated conditional features
        """
        # Concatenate the elements of cond_info according to which_info by their last dimension 
        cond_feats = [cond_info[info] for info in self.which_feat_info]
        # breakpoint()
        return torch.cat(cond_feats, dim=-1)


    @torch.no_grad()
    def filter_pts_by_depthmaps(self, coords, pred_depth_maps, proj_matrices,
                                partial_vol_origin, voxel_size,
                                near, far, depth_interval, d_plane_nums):
        """
        Use the pred_depthmaps to remove redundant pts (pruned by sdf, sdf always have two sides, the back side is useless)
        :param coords: [n, 3]  int coords
        :param pred_depth_maps: [N_views, 1, h, w]
        :param proj_matrices: [N_views, 4, 4]
        :param partial_vol_origin: [3]
        :param voxel_size: 1
        :param near: 1
        :param far: 1
        :param depth_interval: 1
        :param d_plane_nums: 1
        :return:
        """
        device = pred_depth_maps.device
        n_views, _, sizeH, sizeW = pred_depth_maps.shape

        if len(partial_vol_origin.shape) == 1:
            partial_vol_origin = partial_vol_origin[None, :]
        pts = coords * voxel_size + partial_vol_origin

        rs_grid = pts.unsqueeze(0).expand(n_views, -1, -1)
        rs_grid = rs_grid.permute(0, 2, 1).contiguous()  # [n_views, 3, n_pts]
        nV = rs_grid.shape[-1]
        rs_grid = torch.cat([rs_grid, torch.ones([n_views, 1, nV]).to(device)], dim=1)  # [n_views, 4, n_pts]

        # Project grid
        im_p = proj_matrices @ rs_grid  # - transform world pts to image UV space   # [n_views, 4, n_pts]
        im_x, im_y, im_z = im_p[:, 0], im_p[:, 1], im_p[:, 2]
        im_x = im_x / im_z
        im_y = im_y / im_z

        im_grid = torch.stack([2 * im_x / (sizeW - 1) - 1, 2 * im_y / (sizeH - 1) - 1], dim=-1)

        im_grid = im_grid.view(n_views, 1, -1, 2)
        sampled_depths = torch.nn.functional.grid_sample(pred_depth_maps, im_grid, mode='bilinear',
                                                         padding_mode='zeros',
                                                         align_corners=True)[:, 0, 0, :]  # [n_views, n_pts]
        sampled_depths_valid = (sampled_depths > 0.5 * near).float()
        valid_d_min = (sampled_depths - d_plane_nums * depth_interval).clamp(near.item(),
                                                                             far.item()) * sampled_depths_valid
        valid_d_max = (sampled_depths + d_plane_nums * depth_interval).clamp(near.item(),
                                                                             far.item()) * sampled_depths_valid

        mask = im_grid.abs() <= 1
        mask = mask[:, 0]  # [n_views, n_pts, 2]
        mask = (mask.sum(dim=-1) == 2) & (im_z > valid_d_min) & (im_z < valid_d_max)

        mask = mask.view(n_views, -1)
        mask = mask.permute(1, 0).contiguous()  # [num_pts, nviews]

        mask_final = torch.sum(mask.float(), dim=1, keepdim=False) > 0

        return mask_final

    @torch.no_grad()
    def get_valid_sparse_coords_by_sdf_depthfilter(self, sdf_volume, coords_volume, mask_volume, feature_volume,
                                                   pred_depth_maps, proj_matrices,
                                                   partial_vol_origin, voxel_size,
                                                   near, far, depth_interval, d_plane_nums,
                                                   threshold=0.02, maximum_pts=110000):
        """
        assume batch size == 1, from the first lod to get sparse voxels
        :param sdf_volume: [1, X, Y, Z]
        :param coords_volume: [3, X, Y, Z]
        :param mask_volume: [1, X, Y, Z]
        :param feature_volume: [C, X, Y, Z]
        :param threshold:
        :return:
        """
        device = coords_volume.device
        _, dX, dY, dZ = coords_volume.shape

        def prune(sdf_pts, coords_pts, mask_volume, threshold):
            occupancy_mask = (torch.abs(sdf_pts) < threshold).squeeze(1)  # [num_pts]
            valid_coords = coords_pts[occupancy_mask]

            # - filter backside surface by depth maps
            mask_filtered = self.filter_pts_by_depthmaps(valid_coords, pred_depth_maps, proj_matrices,
                                                         partial_vol_origin, voxel_size,
                                                         near, far, depth_interval, d_plane_nums)
            valid_coords = valid_coords[mask_filtered]

            # - dilate
            # occupancy_mask = sparse_to_dense_channel(valid_coords, 1, [dX, dY, dZ], 1, 0, device)  # [dX, dY, dZ, 1]

            # - dilate
            occupancy_mask = occupancy_mask.float()
            occupancy_mask = occupancy_mask.view(1, 1, dX, dY, dZ)
            occupancy_mask = F.avg_pool3d(occupancy_mask, kernel_size=7, stride=1, padding=3)
            occupancy_mask = occupancy_mask.view(-1, 1) > 0

            final_mask = torch.logical_and(mask_volume, occupancy_mask)[:, 0]  # [num_pts]

            return final_mask, torch.sum(final_mask.float())

        C, dX, dY, dZ = feature_volume.shape
        sdf_volume = sdf_volume.permute(1, 2, 3, 0).contiguous().view(-1, 1)
        coords_volume = coords_volume.permute(1, 2, 3, 0).contiguous().view(-1, 3)
        mask_volume = mask_volume.permute(1, 2, 3, 0).contiguous().view(-1, 1)
        feature_volume = feature_volume.permute(1, 2, 3, 0).contiguous().view(-1, C)

        # - for check
        # sdf_volume = torch.rand_like(sdf_volume).float().to(sdf_volume.device) * 0.02

        final_mask, valid_num = prune(sdf_volume, coords_volume, mask_volume, threshold)

        while (valid_num > maximum_pts) and (threshold > 0.003):
            threshold = threshold - 0.002
            final_mask, valid_num = prune(sdf_volume, coords_volume, mask_volume, threshold)

        valid_coords = coords_volume[final_mask]  # [N, 3]
        valid_feature = feature_volume[final_mask]  # [N, C]

        valid_coords = torch.cat([torch.ones([valid_coords.shape[0], 1]).to(valid_coords.device) * 0,
                                  valid_coords], dim=1)  # [N, 4], append batch idx

        # ! if the valid_num is still larger than maximum_pts, sample part of pts
        if valid_num > maximum_pts:
            valid_num = valid_num.long()
            occupancy = torch.ones([valid_num]).to(device) > 0
            choice = np.random.choice(valid_num.cpu().numpy(), valid_num.cpu().numpy() - maximum_pts,
                                      replace=False)
            ind = torch.nonzero(occupancy).to(device)
            occupancy[ind[choice]] = False
            valid_coords = valid_coords[occupancy]
            valid_feature = valid_feature[occupancy]

            # print(threshold, "randomly sample to save memory")

        return valid_coords, valid_feature

    @torch.no_grad()
    def get_valid_sparse_coords_by_sdf(self, sdf_volume, coords_volume, mask_volume, feature_volume, threshold=0.02,
                                       maximum_pts=110000):
        """
        assume batch size == 1, from the first lod to get sparse voxels
        :param sdf_volume: [num_pts, 1]
        :param coords_volume: [3, X, Y, Z]
        :param mask_volume: [1, X, Y, Z]
        :param feature_volume: [C, X, Y, Z]
        :param threshold:
        :return:
        """

        def prune(sdf_volume, mask_volume, threshold):
            occupancy_mask = torch.abs(sdf_volume) < threshold  # [num_pts, 1]

            # - dilate
            occupancy_mask = occupancy_mask.float()
            occupancy_mask = occupancy_mask.view(1, 1, dX, dY, dZ)
            occupancy_mask = F.avg_pool3d(occupancy_mask, kernel_size=7, stride=1, padding=3)
            occupancy_mask = occupancy_mask.view(-1, 1) > 0

            final_mask = torch.logical_and(mask_volume, occupancy_mask)[:, 0]  # [num_pts]

            return final_mask, torch.sum(final_mask.float())

        C, dX, dY, dZ = feature_volume.shape
        coords_volume = coords_volume.permute(1, 2, 3, 0).contiguous().view(-1, 3)
        mask_volume = mask_volume.permute(1, 2, 3, 0).contiguous().view(-1, 1)
        feature_volume = feature_volume.permute(1, 2, 3, 0).contiguous().view(-1, C)

        final_mask, valid_num = prune(sdf_volume, mask_volume, threshold)

        while (valid_num > maximum_pts) and (threshold > 0.003):
            threshold = threshold - 0.002
            final_mask, valid_num = prune(sdf_volume, mask_volume, threshold)

        valid_coords = coords_volume[final_mask]  # [N, 3]
        valid_feature = feature_volume[final_mask]  # [N, C]

        valid_coords = torch.cat([torch.ones([valid_coords.shape[0], 1]).to(valid_coords.device) * 0,
                                  valid_coords], dim=1)  # [N, 4], append batch idx

        # ! if the valid_num is still larger than maximum_pts, sample part of pts
        if valid_num > maximum_pts:
            device = sdf_volume.device
            valid_num = valid_num.long()
            occupancy = torch.ones([valid_num]).to(device) > 0
            choice = np.random.choice(valid_num.cpu().numpy(), valid_num.cpu().numpy() - maximum_pts,
                                      replace=False)
            ind = torch.nonzero(occupancy).to(device)
            occupancy[ind[choice]] = False
            valid_coords = valid_coords[occupancy]
            valid_feature = valid_feature[occupancy]

            # print(threshold, "randomly sample to save memory")

        return valid_coords, valid_feature

    # @torch.no_grad()
    # def extract_fields(self, 
    #                    bound_min: torch.Tensor, 
    #                    bound_max: torch.Tensor, 
    #                    resolution: int, 
    #                    query_func: Callable, 
    #                    device: torch.device,
    #                    ref_poses: Dict[str, torch.Tensor], 
    #                    imgs: torch.Tensor, 
    #                    ref_feats_list: List[torch.Tensor],
    #                    target_pose,
    #                    img_wh: List[int] = [640, 360],
    #                    sampling_strategy: str = 'cubes'
    #                    ) -> np.ndarray:
    #     """ Extract the SDF fields

    #     Args:
    #         bound_min (torch.Tensor): Min bound of the SDF field
    #         bound_max (torch.Tensor): Max bound of the SDF field
    #         resolution (int): Resolution of the SDF field
    #         query_func (Callable): sdf_network.sdf(pts, cond_feats)
    #         device (torch.device): Device to run the query
    #         ref_poses (torch.Tensor): Reference poses
    #         imgs (torch.Tensor): Reference images
    #         ref_feats_list (List[torch.Tensor]): Reference features from GMFlow Encoder
    #         img_wh (Tuple[int, int], optional): Image width and height. Defaults to (640, 360).
    #         sampling_strategy (str, optional): Sampling strategy. Defaults to 'cubes'.

    #     Returns:
    #         np.ndarray: SDF fields
    #     """
    #     N_views = imgs.shape[1]
    #     N = 64

    #     if sampling_strategy == 'cubes':
    #         X = torch.linspace(bound_min[0], bound_max[0], resolution).split(N)
    #         Y = torch.linspace(bound_min[1], bound_max[1], resolution).split(N)
    #         Z = torch.linspace(bound_min[2], bound_max[2], resolution).split(N)

    #         u = np.zeros([resolution, resolution, resolution], dtype=np.float32)
    #         with torch.no_grad():
    #             for xi, xs in enumerate(X):
    #                 for yi, ys in enumerate(Y):
    #                     for zi, zs in enumerate(Z):
    #                         xx, yy, zz = torch.meshgrid(xs, ys, zs)
    #                         pts = torch.cat([xx.reshape(-1, 1), yy.reshape(-1, 1), zz.reshape(-1, 1)], dim=-1).to(device)

    #                         cond_info = self.query_cond_info(pts[None,None,...], ref_poses, imgs, ref_feats_list, device)
    #                         cond_feats = self.query_cond_feats(cond_info)
    #                         output = query_func(pts, cond_feats)
    #                         sdf = output['sdf_pts'].reshape(len(xs), len(ys), len(zs)).detach().cpu().numpy()
    #                         u[xi * N: xi * N + len(xs), yi * N: yi * N + len(ys), zi * N: zi * N + len(zs)] = -1 * sdf

    #                         #u[xi * N: xi * N + len(xs), yi * N: yi * N + len(ys), zi * N: zi * N + len(zs)][empty_mask] = -100
    #     elif sampling_strategy == 'rays':
    #         factor = int(N/2)

    #         # Make grid as vector of 3 coordinates
    #         X = torch.linspace(bound_min[0], bound_max[0], resolution)
    #         Y = torch.linspace(bound_min[1], bound_max[1], resolution).split(factor)
    #         Z = torch.linspace(bound_min[2], bound_max[2], resolution).split(factor)

    #         # Current rays are (2,0,0), let's normalize this already now -> (1,0,0)
    #         vec_from = torch.tensor([1, 0, 0], device=device, dtype=torch.float32)

    #         # Get the direction vector to the target pose
    #         if target_pose is not None:
    #             vec_to, _ = gen_single_ray_from_single_pose(img_wh[1], img_wh[0], target_pose["intrinsics"][0], target_pose["extrinsics"][0])
    #         else: # use one reference pose
    #             vec_to, _ = gen_single_ray_from_single_pose(img_wh[1], img_wh[0], ref_poses["intrinsics"][0], ref_poses["extrinsics"][0])
            
    #         # Compute rotation matrix to rotate the rays to the target pose
    #         rot_matrix = compute_rotation_matrix(vec_to, vec_from)
            
    #         u = np.zeros([resolution, resolution, resolution], dtype=np.float32) - 100 # save as -100 since this is the value of invalid ones
    #         with torch.no_grad(): # Don't iterate thorough X, so that we shoot a ray through the whole box at once
    #             for yi, ys in enumerate(Y):
    #                 for zi, zs in enumerate(Z):
    #                     xx, yy, zz = torch.meshgrid(X, ys, zs)  # Make a grid of X, Y, Z coordinates

    #                     pts = torch.stack([xx, yy, zz], dim=-1).to(device) # (resolution, N/2, N/2)
    #                     pts = pts.permute(1, 2, 0, 3).reshape(1, -1, resolution, 3) # (N/2, N/2, resolution, 3)

    #                     # Rotate the rays to the target pose
    #                     pts_rot = pts.reshape(-1,3) @ rot_matrix # (3, resolution*N/2 * N/2)
    #                     pts = pts_rot.reshape(1, -1, resolution, 3) # (1, N/2*N/2, resolution, 3)

    #                     cond_info = self.query_cond_info(pts, ref_poses, imgs, ref_feats_list, device)
    #                     cond_feats = self.query_cond_feats(cond_info)

    #                     # # Frustum Mask
    #                     # full_mask = (torch.sum(
    #                     #     cond_info['mask_info'].view(-1, N_views).reshape(len(ys), len(zs), len(X), N_views).detach().cpu().permute(2, 0, 1, 3),
    #                     #     dim=-1
    #                     # ) >= cond_info["threshold"]).numpy()

    #                     # empty_mask = np.logical_not(full_mask)
    #                     # # breakpoint()
    #                     # if np.sum(full_mask) != 0: # If there is at least one point in the frustum mask, then query the network
    #                     output = query_func(pts.reshape(-1, 3), cond_feats)#.reshape(-1, cond_feats.shape[-1]))
    #                     # output = query_func(pts[np.repeat(full_mask.reshape(-1, 1), 3, axis=1)].view(-1, 3), 
    #                     #                     cond_feats[torch.tensor(full_mask).view(1,1,-1,1).repeat(1,1,1,22)].view(-1, 22))

    #                     sdf = output['sdf_pts'].reshape(len(ys), len(zs), len(X)).detach().cpu().permute(2, 0, 1).numpy()
    #                     u[:, yi * factor: yi * factor + len(ys), zi * factor: zi * factor + len(zs)] = -1 * sdf

    #         return u, rot_matrix.cpu()
        
    #     return u

    @torch.no_grad()
    def extract_fields(self, 
                       bound_min: torch.Tensor, 
                       bound_max: torch.Tensor, 
                       resolution: int, 
                       query_func: Callable, 
                       device: torch.device,
                       ref_poses: Dict[str, torch.Tensor], 
                       imgs: torch.Tensor, 
                       ref_feats_list: List[torch.Tensor],
                       target_pose,
                       img_wh: List[int] = [640, 360],
                       sampling_strategy: str = 'cubes'
                       ) -> np.ndarray:
        """ Extract the SDF fields

        Args:
            bound_min (torch.Tensor): Min bound of the SDF field
            bound_max (torch.Tensor): Max bound of the SDF field
            resolution (int): Resolution of the SDF field
            query_func (Callable): sdf_network.sdf(pts, cond_feats)
            device (torch.device): Device to run the query
            ref_poses (torch.Tensor): Reference poses
            imgs (torch.Tensor): Reference images
            ref_feats_list (List[torch.Tensor]): Reference features from GMFlow Encoder
            img_wh (Tuple[int, int], optional): Image width and height. Defaults to (640, 360).
            sampling_strategy (str, optional): Sampling strategy. Defaults to 'cubes'.

        Returns:
            np.ndarray: SDF fields
        """
        N_views = imgs.shape[1]
        N = 64

        if sampling_strategy == 'cubes':
            X = torch.linspace(bound_min[0], bound_max[0], resolution).split(N)
            Y = torch.linspace(bound_min[1], bound_max[1], resolution).split(N)
            Z = torch.linspace(bound_min[2], bound_max[2], resolution).split(N)

            u = np.zeros([resolution, resolution, resolution], dtype=np.float32)
            with torch.no_grad():
                for xi, xs in enumerate(X):
                    for yi, ys in enumerate(Y):
                        for zi, zs in enumerate(Z):
                            xx, yy, zz = torch.meshgrid(xs, ys, zs)
                            pts = torch.cat([xx.reshape(-1, 1), yy.reshape(-1, 1), zz.reshape(-1, 1)], dim=-1).to(device)

                            cond_info = self.query_cond_info(pts[None,None,...], ref_poses, imgs, ref_feats_list, device)
                            cond_feats = self.query_cond_feats(cond_info)
                            output = query_func(pts, cond_feats)
                            sdf = output['sdf_pts'].reshape(len(xs), len(ys), len(zs)).detach().cpu().numpy()
                            u[xi * N: xi * N + len(xs), yi * N: yi * N + len(ys), zi * N: zi * N + len(zs)] = -1 * sdf

                            #u[xi * N: xi * N + len(xs), yi * N: yi * N + len(ys), zi * N: zi * N + len(zs)][empty_mask] = -100
        elif sampling_strategy == 'rays':
            # Factor is the stride that we go through the whole grid. We could do all ot oncem but this will be extremely memory hungry.
            factor = int(N/2)

            # Make grid as vector of 3 coordinates
            X = torch.linspace(bound_min[0], bound_max[0], resolution)
            Y = torch.linspace(bound_min[1], bound_max[1], resolution).split(factor)
            Z = torch.linspace(bound_min[2], bound_max[2], resolution).split(factor)

            # Get a reference view
            ref_view = 0 
            extr_matrix = ref_poses["extrinsics"][:, ref_view, :3, :3] # torch.eye(3, device=ref_poses["extrinsics"].device)#
            
            u = np.zeros([resolution, resolution, resolution], dtype=np.float32) - 100 # save as -100 since this is the value of invalid ones
            with torch.no_grad(): # Don't iterate thorough X, so that we shoot a ray through the whole box at once
                for yi, ys in enumerate(Y):
                    for zi, zs in enumerate(Z):
                        xx, yy, zz = torch.meshgrid(X, ys, zs)  # Make a grid of X, Y, Z coordinates
                        # breakpoint()
                        pts = torch.stack([xx, yy, zz], dim=-1).to(device) # (resolution, N/2, N/2)
                        pts = pts.permute(1, 2, 0, 3).reshape(1, -1, resolution, 3) # (N/2, N/2, resolution, 3)

                        # breakpoint()
                        pts_rot = (extr_matrix @ pts.reshape(-1, 3).T).squeeze(0).T # (3, resolution*N/2 * N/2)
                        pts = pts_rot.reshape(1, -1, resolution, 3) # (1, N/2*N/2, resolution, 3)

                        # breakpoint()

                        cond_info = self.query_cond_info(pts, ref_poses, imgs, ref_feats_list, device)
                        cond_feats = self.query_cond_feats(cond_info)

                        # # Frustum Mask
                        # full_mask = (torch.sum(
                        #     cond_info['mask_info'].view(-1, N_views).reshape(len(ys), len(zs), len(X), N_views).detach().cpu().permute(2, 0, 1, 3),
                        #     dim=-1
                        # ) >= cond_info["threshold"]).numpy()

                        # empty_mask = np.logical_not(full_mask)
                        # # breakpoint()
                        # if np.sum(full_mask) != 0: # If there is at least one point in the frustum mask, then query the network
                        output = query_func(pts.reshape(-1, 3), cond_feats)#.reshape(-1, cond_feats.shape[-1]))
                        # output = query_func(pts[np.repeat(full_mask.reshape(-1, 1), 3, axis=1)].view(-1, 3), 
                        #                     cond_feats[torch.tensor(full_mask).view(1,1,-1,1).repeat(1,1,1,22)].view(-1, 22))

                        sdf = output['sdf_pts'].reshape(len(ys), len(zs), len(X)).detach().cpu().permute(2, 0, 1).numpy()
                        u[:, yi * factor: yi * factor + len(ys), zi * factor: zi * factor + len(zs)] = -1 * sdf

                        #u[:, yi * factor: yi * factor + len(ys), zi * factor: zi * factor + len(zs)][empty_mask] = -100
        else:
            raise NotImplementedError(f'Unknown sampling strategy: {sampling_strategy}')
        
        return u, None


    @torch.no_grad()
    def extract_geometry(self, 
                         sdf_network: SparseSdfNetwork, 
                         bound_min: torch.Tensor, 
                         bound_max: torch.Tensor, 
                         resolution: int, 
                         threshold: float, 
                         device: torch.device, 
                         ref_poses: Dict[str, torch.Tensor],
                         imgs: torch.Tensor, 
                         ref_feats_list: List[torch.Tensor],
                         target_pose,
                         img_wh: List[int] = [640, 360],
                         sampling_strategy: str = 'cubes'
                         ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """ Extract geometry from sdf network

        Args:
            sdf_network (SparseSdfNetwork): SDF Network
            bound_min (torch.Tensor): Minimum bound of the bounding box
            bound_max (torch.Tensor): Maximum bound of the bounding box
            resolution (int): Resolution of the bounding box [res, res, res]
            threshold (float): Threshold for marching cube
            device (torch.device): Device
            ref_poses (Dict[str, torch.Tensor]): Reference poses
            imgs (torch.Tensor): Reference images
            ref_feats_list (List[torch.Tensor]): Reference features from GMFLow encoder
            img_wh (List[int], optional): Image width and height. Defaults to [640, 360].
            sampling_strategy (str, optional): Sampling strategy. Defaults to 'cubes'.

        Returns:
            np.ndarray: Vertices
            np.ndarray: Triangles
            np.ndarray: SDF values
        """
        u, rot_mat = self.extract_fields(bound_min, 
                                bound_max, 
                                resolution,
                                lambda pts, cond_feats: sdf_network.sdf(pts, cond_feats),
                                # - sdf need to be multiplied by -1
                                device,
                                ref_poses,
                                imgs, 
                                ref_feats_list,
                                target_pose=target_pose,
                                img_wh=img_wh,
                                sampling_strategy=sampling_strategy
                                )
        vertices, triangles = mcubes.marching_cubes(u, threshold)
        b_max_np = bound_max.detach().cpu().numpy()
        b_min_np = bound_min.detach().cpu().numpy()

        vertices = vertices / (resolution - 1.0) * (b_max_np - b_min_np)[None, :] + b_min_np[None, :]
        return vertices, triangles, u, rot_mat

    @torch.no_grad()
    def extract_depth_maps(self, sdf_network, con_volume, intrinsics, c2ws, H, W, near, far):
        """
        extract depth maps from the density volume
        :param con_volume: [1, 1+C, dX, dY, dZ]  can by con_volume or sdf_volume
        :param c2ws: [B, 4, 4]
        :param H:
        :param W:
        :param near:
        :param far:
        :return:
        """
        device = con_volume.device
        batch_size = intrinsics.shape[0]

        with torch.no_grad():
            ys, xs = torch.meshgrid(torch.linspace(0, H - 1, H),
                                    torch.linspace(0, W - 1, W))  # pytorch's meshgrid has indexing='ij'
            p = torch.stack([xs, ys, torch.ones_like(ys)], dim=-1)  # H, W, 3

            intrinsics_inv = torch.inverse(intrinsics)

            p = p.view(-1, 3).float().to(device)  # N_rays, 3
            p = torch.matmul(intrinsics_inv[:, None, :3, :3], p[:, :, None]).squeeze()  # Batch, N_rays, 3
            rays_v = p / torch.linalg.norm(p, ord=2, dim=-1, keepdim=True)  # Batch, N_rays, 3
            rays_v = torch.matmul(c2ws[:, None, :3, :3], rays_v[:, :, :, None]).squeeze()  # Batch, N_rays, 3
            rays_o = c2ws[:, None, :3, 3].expand(rays_v.shape)  # Batch, N_rays, 3
            rays_d = rays_v

        rays_o = rays_o.contiguous().view(-1, 3)
        rays_d = rays_d.contiguous().view(-1, 3)

        ################## - sphere tracer to extract depth maps               ######################
        depth_masks_sphere, depth_maps_sphere = self.ray_tracer.extract_depth_maps(
            rays_o, rays_d,
            near[None, :].repeat(rays_o.shape[0], 1),
            far[None, :].repeat(rays_o.shape[0], 1),
            sdf_network, con_volume
        )

        depth_maps = depth_maps_sphere.view(batch_size, 1, H, W)
        depth_masks = depth_masks_sphere.view(batch_size, 1, H, W)

        depth_maps = torch.where(depth_masks, depth_maps,
                                 torch.zeros_like(depth_masks.float()).to(device))  # fill invalid pixels by 0

        return depth_maps, depth_masks
