"""
decouple the trainer with the renderer
"""
import os
import cv2 as cv
import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np
import trimesh
import wandb

from utils.misc_utils import visualize_depth_numpy

from utils.training_utils import tocuda, numpy2tensor
from loss.depth_metric import compute_depth_errors

from loss.depth_loss import DepthLoss

from .sparse_neus_renderer import SparseNeuSRenderer

from pyhocon import ConfigTree

# MatchNeRF model libraries
# SparseNeuS model libraries
from models.fields import SingleVarianceNetwork
# from models.trainer_generic import GenericTrainer
from models.sparse_sdf_network import SparseSdfNetwork
from models.rendering_network import GeneralRenderingNetwork
from models.gmflow.gmflow import GMFlow
from typing import List, Dict, Iterator, Optional


class GenericTrainer(nn.Module):
    def __init__(self,
                 encoder: GMFlow,
                 compress_layer: nn.Module,
                 sdf_network: SparseSdfNetwork,
                 variance_network: SingleVarianceNetwork,
                 rendering_network: GeneralRenderingNetwork,
                 n_samples: int,
                 n_importance: int,
                 n_outside: int,
                 perturb: float,
                 alpha_type: str = 'div',
                 conf: Optional[ConfigTree] = None
                 ) -> None:
        super(GenericTrainer, self).__init__()

        self.conf: ConfigTree = conf
        self.base_exp_dir: str = conf['general.base_exp_dir']

        self.anneal_start: float = self.conf.get_float('train.anneal_start', default=0.0)
        self.anneal_end: float = self.conf.get_float('train.anneal_end', default=0.0)

        # network setups
        self.encoder = encoder
        self.compress_layer = compress_layer
        self.sdf_network = sdf_network

        # - warpped by ModuleList to support DataParallel
        self.variance_network = variance_network
        self.rendering_network = rendering_network

        self.n_samples = n_samples
        self.n_importance = n_importance
        self.n_outside = n_outside
        self.num_lods: int = conf.get_int('model.num_lods')  # the number of octree lods
        self.perturb = perturb
        self.alpha_type = alpha_type


        # - the two renderers
        self.sdf_renderer = SparseNeuSRenderer(
            self.sdf_network,
            self.variance_network,
            self.rendering_network,
            self.compress_layer,
            self.n_samples,
            self.n_importance,
            self.n_outside,
            self.perturb,
            alpha_type='div',
            conf=self.conf)

        # sdf network weights
        self.sdf_igr_weight: float = self.conf.get_float('train.sdf_igr_weight')
        self.sdf_sparse_weight: float = self.conf.get_float('train.sdf_sparse_weight', default=0)
        self.sdf_decay_param: float = self.conf.get_float('train.sdf_decay_param', default=100)
        self.fg_bg_weight: float = self.conf.get_float('train.fg_bg_weight', default=0.00)
        self.bg_ratio: float = self.conf.get_float('train.bg_ratio', default=0.0)
        self.depth_weight: float = self.conf.get_float('train.depth_weight', default=0.0)
        self.color_weight: float = self.conf.get_float('train.color_weight', default=0.0)

        self.depth_criterion = DepthLoss()

        # - DataParallel mode, cannot modify attributes in forward()
        self.val_mesh_freq: int = self.conf.get_int('train.val_mesh_freq')

        # Validate mesh resolution
        self.mesh_resolution: int = self.conf.get_int('train.mesh_resolution', default=128)
        self.sampling_strategy: str = self.conf.get_string('train.sampling_strategy', default='cubes')

        # - True for finetuning; False for general training
        self.if_fitted_rendering: bool = self.conf.get_bool('train.if_fitted_rendering', default=False)
        self.prune_depth_filter: bool = self.conf.get_bool('model.prune_depth_filter', default=False)

        self.first: bool = True

    def get_trainable_params(self) -> List[Dict[str, Iterator[torch.nn.Parameter]]]:
        # set trainable params

        params_to_train = []

        params_to_train.append(dict(params = self.encoder.parameters()))

        #  load pretrained featurenet
        params_to_train.append(dict(params = self.sdf_network.parameters()))
        params_to_train.append(dict(params = self.variance_network.parameters()))

        params_to_train.append(dict(params = self.compress_layer.parameters()))

        if self.rendering_network is not None:
            params_to_train.append(dict(params = self.rendering_network.parameters()))

        return params_to_train

    def train_step(self, 
                   sample: Dict[str, torch.Tensor],
                   background_rgb: Optional[torch.Tensor] = None,
                   alpha_inter_ratio: float = 0.0,
                   iter_step: int = 0,
                   ) -> Dict[str, torch.Tensor]:
        # * only support batch_size==1
        # ! attention: the list of string cannot be splited in DataParallel
        batch_idx = sample['batch_idx'][0]
        meta = sample['meta'][batch_idx]  # the scan lighting ref_view info

        sizeW = sample['img_wh'][0][0]
        sizeH = sample['img_wh'][0][1]
        # partial_vol_origin = sample['partial_vol_origin']  # [B, 3]
        near, far = sample['near_fars'][0, 0, :1], sample['near_fars'][0, 0, 1:]

        # the full-size ray variables
        sample_rays = sample['rays']
        rays_o = sample_rays['rays_o'][0]
        rays_d = sample_rays['rays_v'][0]

        imgs = sample['images'][0]
        intrinsics = sample['intrinsics'][0]
        # intrinsics_l_4x = intrinsics.clone()
        # intrinsics_l_4x[:, :2] *= 0.25
        w2cs = sample['w2cs'][0]
        # c2ws = sample['c2ws'][0]
        # proj_matrices = sample['affine_mats']
        scale_mat = sample['scale_mat']
        trans_mat = sample['trans_mat']

        ref_poses = {}
        ref_poses['extrinsics'] = w2cs[None, :-1, :, :]  # B, N, 3, 4
        ref_poses['intrinsics'] = intrinsics[None, :-1]  # B, N, 3, 3
        ref_poses['near_fars'] = sample['near_fars'][:, :-1]  # B, N, 2

        tgt_pose = {}
        tgt_pose['extrinsics'] = w2cs[None, -1, :, :]  # B, 3, 4
        tgt_pose['intrinsics'] = intrinsics[None, -1]  # B, 3, 3
        tgt_pose['near_fars'] = sample['near_fars'][:, -1]  # B, 2
        tgt_pose['c2w'] = sample['c2ws'][:, -1]
        
        tgt_img = imgs[None, -1]
        imgs = imgs[None, :self.conf['dataset.nviews']]

        # Get image features from MatchSDF
        ref_feats_list = self.get_img_feat(
            imgs, attn_splits_list=self.conf['model.encoder.attn_splits_list'],   #NOTE: should check if imgs works here 
            cur_n_src_views=self.conf['dataset.nviews']
        )
        
        # *************** losses        
        loss, losses, depth_statis = None, None, None
        
        # if not self.if_fix_networks:
        render_out = self.sdf_renderer.render(
            rays_o, 
            rays_d, 
            near, 
            far,
            self.sdf_network,
            self.rendering_network,
            ref_feats_list=ref_feats_list,
            imgs=imgs,    
            ref_poses=ref_poses,
            tgt_pose=tgt_pose,
            background_rgb=background_rgb,
            alpha_inter_ratio=alpha_inter_ratio,      # was alpha_inter_ratio_lod0
            # * related to conditional feature
            img_wh=[sizeW, sizeH],
            if_general_rendering=True,
            if_render_with_grad=True,
            iter_step=iter_step,
        )
        render_out.update({"true_depth": sample["query_depth"]})
        loss, losses, depth_statis = self.cal_losses_sdf(render_out, sample_rays, iter_step)

        losses = {
            # - lod 0
            'loss': loss,
            'losses': losses,
            'depth_statis': depth_statis
        }

        return losses
    
    def get_img_feat(self, 
                     imgs: torch.Tensor, 
                     attn_splits_list: Optional[List[int]] = None, 
                     cur_n_src_views: int = 3):
        """ Get image features from MatchSDF

        Args:
            imgs (torch.Tensor): Reference Images 
            attn_splits_list (Optional[List[int]], optional): Attention splits. Defaults to None.
            cur_n_src_views (int, optional): Current number of source views . Defaults to 3.

        Returns:
            List[torch.Tensor]: List of image features 
        """
        if attn_splits_list is None:
            attn_splits_list = self.conf['model.encoder.attn_splits_list']

        # run gmflow backbone to extract features
        out_dict = self.encoder(imgs=imgs, attn_splits_list=attn_splits_list, keep_raw_feats=True,
                                 wo_self_attn=self.conf['model.encoder.wo_self_attn'])
        # split the output
        img_feat_list = []
        index_lists = [(a, b) for a in range(cur_n_src_views - 1) for b in range(a + 1, cur_n_src_views)]
        for scale_idx in range(len(out_dict['aug_feat0s'])):
            img_feat = [[] for _ in range(cur_n_src_views)]
            img1s_feats = out_dict['aug_feat0s'][scale_idx]
            img2s_feats = out_dict['aug_feat1s'][scale_idx]
            for feat_i, (i_idx, j_idx) in enumerate(index_lists):
                img_feat[i_idx].append(img1s_feats[:, feat_i])
                img_feat[j_idx].append(img2s_feats[:, feat_i])
            # post-process the output
            for k, v in enumerate(img_feat):
                img_feat[k] = torch.cat(v, dim=1)
            img_feat = torch.stack(img_feat, dim=1)  # BxVxCxHxW
            img_feat_list.append(img_feat)

        return img_feat_list
    
    def val_step(self, sample,
                 background_rgb=None,
                 alpha_inter_ratio=0,
                 iter_step=0,
                 chunk_size=512,
                 save_vis=False,
                 ):
        # * only support batch_size==1
        # ! attention: the list of string cannot be splited in DataParallel
        batch_idx = sample['batch_idx'][0]
        meta = sample['meta'][batch_idx]  # the scan lighting ref_view info

        sizeW = sample['img_wh'][0][0]
        sizeH = sample['img_wh'][0][1]
        H, W = sizeH, sizeW

        # partial_vol_origin = sample['partial_vol_origin']  # [B, 3]
        near, far = sample['query_near_far'][0, :1], sample['query_near_far'][0, 1:]

        # the ray variables
        sample_rays = sample['rays']
        rays_o = sample_rays['rays_o'][0]
        rays_d = sample_rays['rays_v'][0]
        # rays_ndc_uv = sample_rays['rays_ndc_uv'][0]

        imgs = sample['images'][0]
        intrinsics = sample['intrinsics'][0]
        # intrinsics_l_4x = intrinsics.clone()
        # intrinsics_l_4x[:, :2] *= 0.25
        w2cs = sample['w2cs'][0]
        # c2ws = sample['c2ws'][0]
        # proj_matrices = sample['affine_mats']

        # render_img_idx = sample['render_img_idx'][0]
        # true_img = sample['images'][0][render_img_idx]

        # - the image to render
        scale_mat = sample['scale_mat']  # [1,4,4]  used to convert mesh into true scale
        trans_mat = sample['trans_mat']
        # query_c2w = sample['query_c2w']  # [1,4,4]
        query_w2c = sample['query_w2c']  # [1,4,4]
        true_img = sample['query_image'][0]
        true_img = np.uint8(true_img.permute(1, 2, 0).cpu().numpy() * 255)

        depth_min, depth_max = near.cpu().numpy(), far.cpu().numpy()

        true_depth = sample['query_depth'] if 'query_depth' in sample.keys() else None
        if true_depth is not None:
            true_depth = true_depth[0].cpu().numpy()
            true_depth_colored = visualize_depth_numpy(true_depth, [depth_min, depth_max])[0]
        else:
            true_depth_colored = None
        
        ref_poses = {}
        ref_poses['extrinsics'] = w2cs[None, :-1, :, :]  # B, N, 3, 4
        ref_poses['intrinsics'] = intrinsics[None, :-1]  # B, N, 3, 3
        ref_poses['near_fars'] = sample['near_fars'][:, :-1]  # B, N, 2

        tgt_pose = {}
        tgt_pose['extrinsics'] = w2cs[None, -1, :, :]  # B, 3, 4
        tgt_pose['intrinsics'] = intrinsics[None, -1]  # B, 3, 3
        tgt_pose['near_fars'] = sample['near_fars'][:, -1]  # B, 2
        
        rays_o = rays_o.reshape(-1, 3).split(chunk_size)
        rays_d = rays_d.reshape(-1, 3).split(chunk_size)

        # - obtain conditional features    
        imgs = imgs[None, :self.conf['dataset.test_n_views']]
        with torch.no_grad():
            ref_feats_list = self.get_img_feat(imgs, attn_splits_list=self.conf['model.encoder.attn_splits_list'],   #NOTE: should check if imgs works here
                                           cur_n_src_views=self.conf['dataset.test_n_views'])       #NOTE: weird how +1 takes out of bond here whereas needed in training     

        out_rgb_fine = []
        out_normal_fine = []
        out_depth_fine = []

        # out_depth_fine_explicit = []
        if save_vis:
            for rays_o_batch, rays_d_batch in zip(rays_o, rays_d):

                # ****** lod 0 ****
                render_out = self.sdf_renderer.render(     #was sdf_renderer_lod0
                    rays_o=rays_o_batch, 
                    rays_d=rays_d_batch, 
                    near=near, far=far,
                    sdf_network=self.sdf_network,       #was _lod0
                    rendering_network=self.rendering_network, #was _lod0
                    ref_feats_list=ref_feats_list,
                    imgs=imgs,
                    ref_poses=ref_poses,
                    tgt_pose=tgt_pose,
                    background_rgb=background_rgb,
                    alpha_inter_ratio=alpha_inter_ratio,        #was _lod0
                    # * related to conditional feature
                    # conditional_valid_mask_volume=None,       #NOTE:should be removed next, was con_valid_mask_volume_lod0
                    # * 2d feature maps
                    img_wh=[sizeW, sizeH],
                    if_general_rendering=True,
                    if_render_with_grad=True,
                    )

                feasible = lambda key: ((key in render_out) and (render_out[key] is not None))

                if feasible('depth'):
                    out_depth_fine.append(render_out['depth'].detach().cpu().numpy())

                # if render_out['color_coarse'] is not None:
                if feasible('color_fine'):
                    out_rgb_fine.append(render_out['color_fine'].detach().cpu().numpy())
                if feasible('gradients') and feasible('weights'):
                    if render_out['inside_sphere'] is not None:
                        out_normal_fine.append((render_out['gradients'] * render_out['weights'][:,
                                                                        #   :self.n_samples_lod0 + self.n_importance_lod0,
                                                                          :self.n_samples + self.n_importance ,       
                                                                          None] * render_out['inside_sphere'][
                                                    ..., None]).sum(dim=1).detach().cpu().numpy())
                    else:
                        out_normal_fine.append((render_out['gradients'] * render_out['weights'][:,
                                                                        #   :self.n_samples_lod0 + self.n_importance_lod0,
                                                                            :self.n_samples + self.n_importance,
                                                                          None]).sum(dim=1).detach().cpu().numpy())
                del render_out

            # - save visualization
            self.save_visualization(true_img, true_depth_colored, out_depth_fine, out_normal_fine,
                                    query_w2c[0], out_rgb_fine, H, W,
                                    depth_min, depth_max, iter_step, meta, "val", first=self.first)
            

        # # - extract mesh
        if (iter_step % self.val_mesh_freq == 0):
            torch.cuda.empty_cache()
            self.validate_mesh(threshold=0,
                               ref_poses=ref_poses, 
                               imgs=imgs, 
                               ref_feats_list=ref_feats_list,
                               target_pose=tgt_pose,
                               device=rays_o_batch.device,
                               # occupancy_mask=con_valid_mask_volume_lod0[0, 0],
                               mode='val_bg', 
                               meta=meta,
                               iter_step=iter_step, 
                               scale_mat=scale_mat, 
                               trans_mat=trans_mat,
                               img_wh=[sizeW, sizeH])
            torch.cuda.empty_cache()

    def save_visualization(self, 
                           true_img: np.ndarray, 
                           true_colored_depth: np.ndarray, 
                           out_depth: np.ndarray, 
                           out_normal: np.ndarray, 
                           w2cs: np.ndarray, 
                           out_color: np.ndarray, 
                           H: int, 
                           W: int,
                           depth_min: int, 
                           depth_max: int, 
                           iter_step: int, 
                           meta, 
                           comment: str, 
                           out_color_mlp: Optional[List[np.ndarray]] = [], 
                           first: bool = True):
        if len(out_color) > 0:
            img_fine = (np.concatenate(out_color, axis=0).reshape([H, W, 3]) * 256).clip(0, 255)

        if len(out_color_mlp) > 0:
            img_mlp = (np.concatenate(out_color_mlp, axis=0).reshape([H, W, 3]) * 256).clip(0, 255)

        if len(out_normal) > 0:
            normal_img = np.concatenate(out_normal, axis=0)
            rot = w2cs[:3, :3].detach().cpu().numpy()
            # - convert normal from world space to camera space
            normal_img = (np.matmul(rot[None, :, :],
                                    normal_img[:, :, None]).reshape([H, W, 3]) * 128 + 128).clip(0, 255)
        wandb_img_dct = {}
        if len(out_depth) > 0:
            pred_depth = np.concatenate(out_depth, axis=0).reshape([H, W])
            pred_depth_colored = visualize_depth_numpy(pred_depth, [depth_min, depth_max])[0]
            os.makedirs(os.path.join(self.base_exp_dir, 'depths_' + comment), exist_ok=True)
            if true_colored_depth is not None:
                out_depth_img = np.concatenate(
                        [true_colored_depth, pred_depth_colored, true_img])[:, :, ::-1]
                cv.imwrite(
                    os.path.join(self.base_exp_dir, 'depths_' + comment,
                                 '{:0>8d}_{}.png'.format(iter_step, meta)), out_depth_img)
            else:
                out_depth_img = np.concatenate([pred_depth_colored, true_img])[:, :, ::-1]
                cv.imwrite(
                    os.path.join(self.base_exp_dir, 'depths_' + comment,
                                 '{:0>8d}_{}.png'.format(iter_step, meta)), out_depth_img)
            wandb_img_dct['depth'] = wandb.Image(out_depth_img, caption='depth')
        if len(out_color) > 0:
            out_color_img = np.concatenate([img_fine, true_img])[:, :, ::-1]
            os.makedirs(os.path.join(self.base_exp_dir, 'synthesized_color_' + comment), exist_ok=True)
            cv.imwrite(os.path.join(self.base_exp_dir, 'synthesized_color_' + comment,
                                    '{:0>8d}_{}.png'.format(iter_step, meta)), out_color_img)  # bgr2rgb
            wandb_img_dct['synthesized_color'] = wandb.Image(out_color_img, caption='synthesized_color')

        if len(out_color_mlp) > 0:
            out_color_mlp_img = np.concatenate([img_mlp, true_img])[:, :, ::-1]
            os.makedirs(os.path.join(self.base_exp_dir, 'synthesized_color_mlp_' + comment), exist_ok=True)
            cv.imwrite(os.path.join(self.base_exp_dir, 'synthesized_color_mlp_' + comment,
                                    '{:0>8d}_{}.png'.format(iter_step, meta)), out_color_mlp_img)  # bgr2rgb
            wandb_img_dct['synthesized_color_mlp'] = wandb.Image(out_color_mlp_img, caption='synthesized_color_mlp')

        if len(out_normal) > 0:
            out_normal_img = normal_img[:, :, ::-1]
            os.makedirs(os.path.join(self.base_exp_dir, 'normals_' + comment), exist_ok=True)
            cv.imwrite(os.path.join(self.base_exp_dir, 'normals_' + comment,
                                    '{:0>8d}_{}.png'.format(iter_step, meta)), out_normal_img)  # bgr2rgb
            wandb_img_dct['normals'] = wandb.Image(out_normal_img, caption='normals')
        
        if self.first:
            wandb.log(wandb_img_dct)

    def forward(self, 
                sample: Dict[str, torch.Tensor],
                background_rgb: Optional[torch.Tensor] = None,
                alpha_inter_ratio: float = 0.0, 
                iter_step: int = 0,
                mode: str = 'train',
                save_vis: bool = False,
                ):

        if mode == 'train':
            self.first  = True
            return self.train_step(sample,
                                   background_rgb=background_rgb,
                                   alpha_inter_ratio=alpha_inter_ratio,
                                   iter_step=iter_step
                                   )
        elif mode == 'val':
            return self.val_step(sample,
                                 background_rgb=background_rgb,
                                 alpha_inter_ratio=alpha_inter_ratio,
                                 iter_step=iter_step,
                                 save_vis=save_vis,
                                 )
        elif mode == 'mesh':
            return self.mesh_step(sample, iter_step=iter_step)

    def cal_losses_sdf(self, 
                       render_out: Dict[str, torch.Tensor], 
                       sample_rays: Dict[str, torch.Tensor], 
                       iter_step: Optional[int] = -1):
        """ Calculate the losses for SDF prediction

        Args:
            render_out (Dict[str, torch.Tensor]): Output of the renderer
            sample_rays (Dict[str, torch.Tensor]): Sampled rays
            iter_step (Optional[int], optional): Iteration Step. Defaults to -1.

        Returns:
            Dict[str, torch.Tensor]: Losses 
        """

        # loss weight schedule; the regularization terms should be added in later training stage
        def get_weight(iter_step, weight):
            anneal_start = self.anneal_start
            anneal_end = self.anneal_end
            anneal_end = anneal_end * 2

            if iter_step < 0:
                return weight

            if anneal_end == 0.0:
                return weight
            elif iter_step < anneal_start:
                return 0.0
            else:
                return np.min(
                    [1.0,
                     (iter_step - anneal_start) / (anneal_end - anneal_start)]) * weight

        rays_o = sample_rays['rays_o'][0]
        rays_d = sample_rays['rays_v'][0]
        true_rgb = sample_rays['rays_color'][0]

        if 'rays_depth' in sample_rays.keys():
            true_depth = sample_rays['rays_depth'][0]
        else:
            true_depth = None
        mask = sample_rays['rays_mask'][0]

        color_fine = render_out['color_fine']
        color_fine_mask = render_out['color_fine_mask']
        depth_pred = render_out['depth']

        variance = render_out['variance']
        cdf_fine = render_out['cdf_fine']
        weight_sum = render_out['weights_sum']

        gradient_error_fine = render_out['gradient_error_fine']

        sdf = render_out['sdf']

        # * color generated by mlp
        color_mlp = render_out['color_mlp']
        color_mlp_mask = render_out['color_mlp_mask']

        if color_fine is not None:
            # Color loss
            color_mask = color_fine_mask if color_fine_mask is not None else mask
            color_mask = color_mask[..., 0]
            color_error = (color_fine[color_mask] - true_rgb[color_mask])
            color_fine_loss = F.l1_loss(color_error, torch.zeros_like(color_error).to(color_error.device),
                                        reduction='mean')

            psnr = 20.0 * torch.log10(
                1.0 / (((color_fine[color_mask] - true_rgb[color_mask]) ** 2).mean() / (3.0)).sqrt())
        else:
            color_fine_loss = 0.
            psnr = 0.

        if color_mlp is not None:
            # Color loss
            color_mlp_mask = color_mlp_mask[..., 0]
            color_error_mlp = (color_mlp[color_mlp_mask] - true_rgb[color_mlp_mask])
            color_mlp_loss = F.l1_loss(color_error_mlp,
                                       torch.zeros_like(color_error_mlp).to(color_error_mlp.device),
                                       reduction='mean')

            psnr_mlp = 20.0 * torch.log10(
                1.0 / (((color_mlp[color_mlp_mask] - true_rgb[color_mlp_mask]) ** 2).mean() / (3.0)).sqrt())
        else:
            color_mlp_loss = 0.
            psnr_mlp = 0.

        # depth loss is only used for inference, not included in total loss
        if true_depth is not None:
            # breakpoint()
            depth_loss = self.depth_criterion(depth_pred, true_depth, mask)

            # depth evaluation
            depth_statis = compute_depth_errors(depth_pred.detach().cpu().numpy(), true_depth.cpu().numpy(),
                                                mask.cpu().numpy() > 0)
            depth_statis = numpy2tensor(depth_statis, device=rays_o.device)
        else:
            depth_loss = 0.
            depth_statis = None

        sparse_loss_1 = torch.exp(
            -1 * torch.abs(render_out['sdf_random']) * self.sdf_decay_param).mean()  # - should equal
        sparse_loss_2 = torch.exp(-1 * torch.abs(sdf) * self.sdf_decay_param).mean()
        sparse_loss = (sparse_loss_1 + sparse_loss_2) / 2

        sdf_mean = torch.abs(sdf).mean()
        sparseness_1 = (torch.abs(sdf) < 0.01).to(torch.float32).mean()
        sparseness_2 = (torch.abs(sdf) < 0.02).to(torch.float32).mean()

        # Eikonal loss
        gradient_error_loss = gradient_error_fine

        # Mask loss, optional
        # The images of DTU dataset contain large black regions (0 rgb values),
        # can use this data prior to make fg more clean
        background_loss = 0.0
        fg_bg_loss = 0.0
        if self.fg_bg_weight > 0 and torch.mean((mask < 0.5).to(torch.float32)) > 0.02:
            weights_sum_fg = render_out['weights_sum_fg']
            fg_bg_error = (weights_sum_fg - mask)[mask < 0.5]
            fg_bg_loss = F.l1_loss(fg_bg_error,
                                   torch.zeros_like(fg_bg_error).to(fg_bg_error.device),
                                   reduction='mean')

        # ! the first 50k, don't use bg constraint
        # fg_bg_weight = 0.0 if iter_step < 50000 else get_weight(iter_step, self.fg_bg_weight)
        # sparse_weight = get_weight(iter_step, self.sdf_sparse_weight)
        sparse_weight = self.sdf_sparse_weight
        fg_bg_weight = self.fg_bg_weight

        # print(sparse_weight, fg_bg_weight, self.sdf_igr_weight, self.depth_weight)

        loss = (color_fine_loss + color_mlp_loss) * self.color_weight + \
               sparse_loss * sparse_weight + \
               fg_bg_loss * fg_bg_weight + \
               gradient_error_loss * self.sdf_igr_weight + \
               depth_loss * self.depth_weight
        #    sparse_loss * get_weight(iter_step, self.sdf_sparse_weight) + \
        # + depth_loss * self.depth_weight

        losses = {
            "loss": loss,
            "depth_loss": depth_loss,
            "color_fine_loss": color_fine_loss,
            "color_mlp_loss": color_mlp_loss,
            "gradient_error_loss": gradient_error_loss,
            "background_loss": background_loss,
            "sparse_loss": sparse_loss,
            "sparseness_1": sparseness_1,
            "sparseness_2": sparseness_2,
            "sdf_mean": sdf_mean,
            "psnr": psnr,
            "psnr_mlp": psnr_mlp,
            "weights_sum": render_out['weights_sum'],
            "weights_sum_fg": render_out['weights_sum_fg'],
            "alpha_sum": render_out['alpha_sum'],
            "variance": render_out['variance'],
            "sparse_weight": sparse_weight,
            "fg_bg_weight": fg_bg_weight,
            "fg_bg_loss": fg_bg_loss
        }

        losses = numpy2tensor(losses, device=rays_o.device)
        return loss, losses, depth_statis

    def validate_mesh(self, 
                      ref_poses: Dict[str, torch.Tensor], 
                      imgs: torch.Tensor, 
                      ref_feats_list: List[torch.Tensor],
                      target_pose,
                      threshold: float = 0.0, 
                      mode: str = 'val',
                      device: torch.DeviceObjType = torch.device('cuda'),
                      meta: str = '', 
                      iter_step: int = 0,
                      bound_min: torch.Tensor = torch.tensor([-1, -1, -1], dtype=torch.float32), 
                      bound_max: torch.Tensor = torch.tensor([1, 1, 1], dtype=torch.float32),
                      scale_mat: Optional[torch.Tensor] = None,
                      trans_mat: Optional[torch.Tensor] = None,
                      img_wh: Optional[List[int]] = [640, 360],
                      ):
        """ Generates a mesh inside the bounding box of the scene

        Args:
            ref_poses (Dict[str, torch.Tensor]): Rerence poses (extrinsics, intrinsics, near_fars)
            imgs (torch.Tensor): Reference images
            ref_feats_list (List[torch.Tensor]): Reference features from GMFLow backbone
            threshold (float, optional): . Defaults to 0.0.
            mode (str, optional): Only for the name. Defaults to 'val'.
            device (torch.DeviceObjType, optional): Device to execute it on. Defaults to torch.device('gpu').
            meta (str, optional): Only for the name. Defaults to ''.
            iter_step (int, optional): Only for the name. Defaults to 0.
            bound_min (torch.Tensor, optional): Minimum bound of the mesh. Defaults to torch.tensor([-1, -1, -1], dtype=torch.float32).
            bound_max (torch.Tensor, optional): Maximum bound of the mesh. Defaults to torch.tensor([1, 1, 1], dtype=torch.float32).
            scale_mat (Optional[torch.Tensor], optional): Scale Matrix. Defaults to None.
            trans_mat (Optional[torch.Tensor], optional): Transformation Matrix. Defaults to None.
            img_wh (Optional[List[int]], optional): Image width and height. Defaults to [640, 360].
        """
        resolution = self.mesh_resolution
        sampling_strategy = self.sampling_strategy
        vertices, triangles, _, _ = self.sdf_renderer.extract_geometry(
            sdf_network=self.sdf_network,
            bound_min=bound_min, 
            bound_max=bound_max, 
            resolution=resolution,
            threshold=threshold, 
            device=device,
            ref_poses=ref_poses, 
            imgs=imgs, 
            ref_feats_list=ref_feats_list,
            target_pose=target_pose,
            img_wh=img_wh,
            sampling_strategy=sampling_strategy,
        )

        # Scale the mesh
        if scale_mat is not None:
            scale_mat_np = scale_mat.cpu().numpy()
            vertices = vertices * scale_mat_np[0][0, 0] + scale_mat_np[0][:3, 3][None]

        # Transform the mesh
        if trans_mat is not None:
            trans_mat_np = trans_mat.cpu().numpy()
            vertices_homo = np.concatenate([vertices, np.ones_like(vertices[:, :1])], axis=1)
            vertices = np.matmul(trans_mat_np, vertices_homo[:, :, None])[:, :3, 0]

        mesh = trimesh.Trimesh(vertices, triangles)
        os.makedirs(os.path.join(self.base_exp_dir, 'meshes_' + mode), exist_ok=True)
        mesh.export(os.path.join(self.base_exp_dir, 'meshes_' + mode,
                                 'mesh_{:0>8d}_{}.ply'.format(iter_step, meta)))


    def mesh_step(self, 
                  sample: Dict[str, torch.Tensor],
                  iter_step: int = 0,
                  ) -> Dict[str, torch.Tensor]:
        batch_idx = sample['batch_idx'][0]
        meta = sample['meta'][batch_idx]  # the scan lighting ref_view info

        imgs = sample['images'][0]
        intrinsics = sample['intrinsics'][0]
        w2cs = sample['w2cs'][0]
        scale_mat = sample['scale_mat']
        trans_mat = sample['trans_mat']

        sizeW = sample['img_wh'][0][0]
        sizeH = sample['img_wh'][0][1]

        ref_poses = {}
        ref_poses['extrinsics'] = w2cs[None, :-1, :, :]  # B, N, 3, 4
        ref_poses['intrinsics'] = intrinsics[None, :-1]  # B, N, 3, 3
        ref_poses['near_fars'] = sample['near_fars'][:, :-1]  # B, N, 2

        # tgt_img = imgs[None, -1]
        imgs = imgs[None, :self.conf['dataset.nviews']]

        with torch.no_grad():
            ref_feats_list = self.get_img_feat(
                imgs, attn_splits_list=self.conf['model.encoder.attn_splits_list'],
                cur_n_src_views=self.conf['dataset.test_n_views']
            )    

        # # - extract mesh
        torch.cuda.empty_cache()
        self.validate_mesh(threshold=0,
                            ref_poses=ref_poses, 
                            imgs=imgs, 
                            ref_feats_list=ref_feats_list,
                            target_pose=None,
                            device=imgs.device,
                            # occupancy_mask=con_valid_mask_volume_lod0[0, 0],
                            mode='train_bg', 
                            meta=meta,
                            iter_step=iter_step, 
                            scale_mat=scale_mat, 
                            trans_mat=trans_mat,
                            img_wh=[sizeW, sizeH])
        torch.cuda.empty_cache()