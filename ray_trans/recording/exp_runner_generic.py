from misc.utils import load_gmflow_checkpoint
import torch
import utils

# Added this snippet (https://github.com/pytorch/pytorch/issues/90613) 
# to solve the multi-threading issues with torch.inverse function. 
# It does not working for multiple GPU training.
a = torch.inverse(torch.ones((0, 0), device="cuda:0"))

import torch.nn.functional as F
import torch.nn as nn
from torch.utils.data import DataLoader
import argparse
import os
import logging
import numpy as np
import cv2 as cv
import trimesh
from shutil import copyfile
import wandb

from icecream import ic
from tqdm import tqdm
from pyhocon import ConfigFactory, ConfigTree

# SparseNeuS model libraries
from models.fields import SingleVarianceNetwork
from models.trainer_generic import GenericTrainer
from models.sparse_sdf_network import SparseSdfNetwork
from models.rendering_network import GeneralRenderingNetwork
from data.dtu_general import MVSDatasetDtuPerView

# MatchNeRF model libraries
from models.gmflow.gmflow import GMFlow
import shutil

# from utils.training_utils import tocuda

from termcolor import colored
from typing import List, Tuple, Union, Dict, Any, Optional, Iterator


# Check if torch.compile exists:
if torch.__version__ >= '2.0.0':
    TORCH_COMPILE_CAPABILITY = True
    device_props = torch.cuda.get_device_properties(torch.device("cuda"))
    if device_props.major < 7:
        TORCH_COMPILE_CAPABILITY = False
else:
    TORCH_COMPILE_CAPABILITY = False


def count_params(trainables):
    """Counts total number of parameters"""
    num_params = []
    for model in trainables:
        model_parameters = filter(lambda p: p.requires_grad, model.parameters())
        params = sum([np.prod(p.size()) for p in model_parameters])
        num_params.append(int(params))

    return num_params


class Runner:
    def __init__(self, 
                 conf_path: str, 
                 mode: str = 'train', 
                 is_continue: bool = False,
                 is_restore: bool = False, 
                 local_rank: int = 0
                 ) -> None:

        # Initial setting
        self.device: torch.DeviceObjType = torch.device('cuda:%d' % local_rank)
        self.num_devices: int = torch.cuda.device_count()
        self.mode = mode
        self.is_continue = is_continue
        self.is_restore = is_restore
        self.logger: logging.Logger = logging.getLogger('exp_logger')

        print(colored("detected %d GPUs" % self.num_devices, "red"))

        self.conf_path: str = conf_path
        self.conf: ConfigTree = ConfigFactory.parse_file(conf_path)

        self.base_exp_dir: str = self.conf['general.base_exp_dir']
        print(colored(f"base_exp_dir:  {self.base_exp_dir}", 'yellow'))
        
        os.makedirs(self.base_exp_dir, exist_ok=True)

        if not os.path.isdir(os.path.join(self.base_exp_dir, "checkpoints")):
            os.makedirs(os.path.join(self.base_exp_dir, "checkpoints"), exist_ok=True)
            copyfile('/kuacc/users/ecetin17/matchnerfckpt_000000.pth', 
                     os.path.join(os.path.join(self.base_exp_dir, "checkpoints",'ckpt_000000.pth')))
            print(colored("copied checkpoint file from SparseNeuS--MatchSDF ckpt_000000.pth", "green"))
        
        self.iter_step: int = 0
        self.val_step: int = 0

        # Training parameters inherited from SparseNeuS
        self.end_iter: int = self.conf.get_int('train.end_iter')
        self.save_freq: int = self.conf.get_int('train.save_freq')
        self.report_freq: int = self.conf.get_int('train.report_freq')
        self.val_freq: int = self.conf.get_int('train.val_freq')
        self.val_mesh_freq: int = self.conf.get_int('train.val_mesh_freq')
        self.batch_size: int = self.num_devices  # use DataParallel to warp
        self.validate_resolution_level: int = self.conf.get_int('train.validate_resolution_level')
        self.learning_rate: float = self.conf.get_float('train.learning_rate')

        self.learning_rate_enc: float = self.conf.get_float('train.learning_rate_enc')

        self.learning_rate_milestone: List[int] = self.conf.get_list('train.learning_rate_milestone')
        self.learning_rate_factor: float = self.conf.get_float('train.learning_rate_factor')
        self.use_white_bkgd: bool = self.conf.get_bool('train.use_white_bkgd')
        self.N_rays: int = self.conf.get_int('train.N_rays')

        # Warmup params for sdf gradient inherited from SparseNeuS
        self.anneal_start: float = self.conf.get_float('train.anneal_start', default=0)
        self.anneal_end: float = self.conf.get_float('train.anneal_end', default=0)

        # Build MatchNeRF Networks 
        self.build_encoder()
        
        # Build SparseNeuS Networks
        self.build_decoder()

        # Renderer model
        self.trainer: GenericTrainer = GenericTrainer(
            self.encoder,
            self.compress_layer,
            self.sdf_network,
            self.variance_network,
            self.rendering_network,
            **self.conf['model.trainer'],
            conf=self.conf
        )

        self.optimizer_setup()
        
        # Load checkpoint
        latest_model_name = None

        if is_continue:
            model_list_raw = os.listdir(os.path.join(self.base_exp_dir, 'checkpoints'))
            model_list = []
            for model_name in model_list_raw:
                if model_name.startswith('ckpt'):
                    if model_name[-3:] == 'pth':  # and int(model_name[5:-4]) <= self.end_iter:
                        model_list.append(model_name)
            model_list.sort()
            latest_model_name = model_list[-1]

        if latest_model_name is not None:
            self.logger.info('Find checkpoint: {}'.format(latest_model_name))
            self.load_checkpoint(latest_model_name)
        
        num_params = count_params(
            [self.encoder, self.sdf_network, 
             self.variance_network, self.rendering_network]
        )
        print(colored(f"The model has {num_params} parameters", 'yellow'))

        # Data and optimizer setup
        self.data_setup()

        self.trainer: GenericTrainer = torch.nn.DataParallel(self.trainer).to(self.device)

        if self.mode[:5] == 'train':
            self.file_backup()


    def build_encoder(self) -> None:
        """Builds the MatchNeRF modules that are used for encoding"""
        print(colored("Building MatchNeRF encoder networks", 'yellow'))

        feat_enc = GMFlow(
            feature_channels=self.conf['model.encoder.feature_channels'], 
            num_scales=self.conf['model.encoder.num_scales'], 
            num_head=self.conf['model.encoder.num_head'],
            attention_type='swin', 
            ffn_dim_expansion=self.conf['model.encoder.ffn_dim_expansion'],
            feature_upsampler=self.conf['model.encoder.feature_upsampler'],
            upsample_factor=self.conf['model.encoder.upsample_factor'],
            num_transformer_layers=self.conf['model.encoder.num_transformer_layers'],
            device=self.conf)
        if TORCH_COMPILE_CAPABILITY:
            self.encoder: GMFlow = torch.compile(feat_enc).to(self.device)
        else:
            self.encoder: GMFlow = feat_enc.to(self.device)


    def build_decoder(self):
        """Builds the SparseNeus modules that are used for decoding"""
        print(colored("Building SparseNeus decoder networks", 'yellow'))

        # Get the feature size for the decoder
        nviews = self.conf.get_int('dataset.nviews') 
        which_feat_info = self.conf.get_list('model.which_feat_info', default=['feat_info', 'color_info', 'mask_info'])
        cos_n_group = self.conf.get_list('model.encoder.cos_n_group', default=[2, 8])
        
        # Compress layer
        first_compress_size = self.conf.get_int('model.compress.first_compression_channels', default=32)
        compression_size = self.conf.get_int('model.compress.out_channels', default=16)
        activate_out = self.conf.get_bool('model.compress.activate_out', default=True)
        cat_scales = self.conf.get_bool('model.compress.cat_scales', default=True)
        use_grouped_variance = self.conf.get_bool('model.use_grouped_variance', default=False)

        
        # Compute SDF layer size
        similarity_size = sum(cos_n_group) + sum(cos_n_group) * use_grouped_variance #NOTE: times 2 because doing grouped variance as well now
        feat_size = 0
        for feat_info in which_feat_info:
            if feat_info == 'feat_info':
                feat_size += similarity_size
            elif feat_info == 'color_info':
                feat_size += 3 * nviews
            elif feat_info == 'mask_info':
                feat_size += nviews
            elif feat_info == 'viewpair_mask':
                feat_size += 1
            elif feat_info == 'new_feat':
                # add_size = compression_size if not cat_scales else compression_size * len(cos_n_group)
                add_size = compression_size if not cat_scales else compression_size
                feat_size += add_size
            else:
                pass
                # feat_size += compression_size # 16 default
        
        # Set the feature size for the decoder
        self.sdf_network: SparseSdfNetwork = SparseSdfNetwork(ch_cond_feature=feat_size,
            **self.conf['model.sdf_network']).to(self.device)
        self.variance_network: SingleVarianceNetwork = SingleVarianceNetwork(
            **self.conf['model.variance_network']).to(self.device)

        self.rendering_network: GeneralRenderingNetwork = GeneralRenderingNetwork(
            **self.conf['model.rendering_network']).to(self.device)
        
        # Make Compress Layer
        if activate_out:
            self.compress_layer = nn.Sequential(
                nn.Linear(self.conf['model.encoder.feature_channels']*2, first_compress_size),
                nn.ReLU(),
                nn.Linear(first_compress_size, compression_size),
                nn.ReLU()
            ).to(self.device)
        else:
            self.compress_layer = nn.Sequential(
                nn.Linear(self.conf['model.encoder.feature_channels']*2, first_compress_size),
                nn.ReLU(),
                nn.Linear(first_compress_size, compression_size),
            ).to(self.device)

        if TORCH_COMPILE_CAPABILITY:
            self.sdf_network: SparseSdfNetwork = torch.compile(self.sdf_network).to(self.device)
            self.variance_network: SingleVarianceNetwork = torch.compile(self.variance_network).to(self.device)
            self.rendering_network: GeneralRenderingNetwork = torch.compile(self.rendering_network).to(self.device)
            self.compress_layer: nn.Sequential = torch.compile(self.compress_layer).to(self.device)


    def optimizer_setup(self):
        """
        Setup Optimizer for MatchSDF
        """
        params_to_train_dct = self.trainer.get_trainable_params()
        enc_ = params_to_train_dct[0]
        enc_['lr'] = self.learning_rate_enc

        sdf_ = params_to_train_dct[1]
        sdf_['lr'] = self.learning_rate
        var_ = params_to_train_dct[2]
        var_['lr'] = self.learning_rate
        compress_ = params_to_train_dct[3]
        compress_['lr'] = self.learning_rate
        render_ = params_to_train_dct[4]
        render_['lr'] = self.learning_rate
        self.optimizer = torch.optim.Adam([enc_, sdf_, var_, compress_, render_])

        self.params_to_train: List[Dict[str, Union[int, Iterator[torch.nn.Parameter]]]] = [enc_, sdf_, var_, render_]
        self.param_groups: Dict[str, int] = {'enc': 0, 'sdf_net': 1, 'var_net': 2, 'renderer': 3}

    def data_setup(self):
        """
        if use ddp, use setup() not prepare_data(),
        prepare_data() only called on 1 GPU/TPU in distributed
        :return:
        """
        self.train_dataset: MVSDatasetDtuPerView = MVSDatasetDtuPerView(
            root_dir=self.conf['dataset.trainpath'],
            split=self.conf.get_string('dataset.train_split', default='train'),
            split_filepath=self.conf.get_string('dataset.train_split_filepath', default=None),
            n_views=self.conf['dataset.nviews'],
            downSample=self.conf['dataset.imgScale_train'],
            N_rays=self.N_rays,
            batch_size=self.batch_size,
            clean_image=True,  # True for training
            importance_sample=self.conf.get_bool('dataset.importance_sample', default=False),
            do_single_scan=self.conf.get_bool('dataset.train_do_single_scan', default=False),
            scan_id=self.conf.get_int('dataset.train_scan_id', default=19),
        )
        self.val_dataset: MVSDatasetDtuPerView = MVSDatasetDtuPerView(
            root_dir=self.conf['dataset.valpath'],
            split=self.conf.get_string('dataset.test_split', default='test'),
            split_filepath=self.conf.get_string('dataset.val_split_filepath', default=None),
            n_views=self.conf['dataset.test_n_views'],      #was 3
            downSample=self.conf['dataset.imgScale_test'],
            N_rays=self.N_rays,
            batch_size=self.batch_size,
            clean_image=self.conf.get_bool('dataset.mask_out_image',
                                           default=False) if self.mode != 'train' else False,
            importance_sample=self.conf.get_bool('dataset.importance_sample', default=False),
            test_ref_views=self.conf.get_list('dataset.test_ref_views', default=[]),
            do_single_scan=self.conf.get_bool('dataset.test_do_single_scan', default=False),
            scan_id=self.conf.get_int('dataset.test_scan_id', default=40),
        )

        self.train_dataloader: DataLoader = DataLoader(
            self.train_dataset, shuffle=True, num_workers=4 * self.batch_size,
            batch_size=self.batch_size, pin_memory=True, drop_last=True
        )
        self.val_dataloader: DataLoader = DataLoader(
            self.val_dataset, shuffle=False if self.mode == 'train' else True, 
            num_workers=4 * self.batch_size, batch_size=self.batch_size, pin_memory=True, drop_last=True
        )
        self.val_dataloader_iterator: Iterator[DataLoader] = iter(self.val_dataloader)


    def train(self):
        res_step = self.end_iter - self.iter_step
        epochs = int(1 + res_step // len(self.train_dataloader))

        self.adjust_learning_rate()
        print(colored("starting training learning rate: {:.5f}".format(self.optimizer.param_groups[0]['lr']), "yellow"))

        background_rgb = None
        if self.use_white_bkgd:
            background_rgb = torch.ones([1, 3]).to(self.device)
       
        for epoch_i in range(epochs):
            print(colored("current epoch %d" % epoch_i, 'red'))

            for batch in tqdm(self.train_dataloader):
                self.optimizer.zero_grad()
                batch['batch_idx'] = torch.tensor([x for x in range(self.batch_size)])  # used to get meta

                # - warmup params
                alpha_inter_ratio = self.get_alpha_inter_ratio(self.anneal_start, self.anneal_end)
                
                losses = self.trainer(
                    batch,
                    background_rgb=background_rgb,
                    alpha_inter_ratio=alpha_inter_ratio,
                    iter_step=self.iter_step,
                    mode='train',
                )

                loss = losses['loss'].mean()
                losses = losses['losses']

                self.optimizer.zero_grad()
                loss.backward() 


                for param_dict in self.params_to_train:
                    if isinstance(param_dict, dict):
                        torch.nn.utils.clip_grad_norm_(param_dict['params'], 1.0)

                self.optimizer.step()

                if self.iter_step % self.val_mesh_freq == 0:
                    self.trainer(
                        batch, iter_step=self.iter_step, mode='mesh'
                    )
                
                self.iter_step += 1

                if self.iter_step % self.report_freq == 0:
                    self.report(loss, losses, alpha_inter_ratio)

                if self.iter_step % self.save_freq == 0 and self.iter_step > 0:
                    self.save_checkpoint()

                if self.iter_step % self.val_freq == 0:
                    self.validate()

                # - ajust learning rate
                self.adjust_learning_rate()

    def report(self,
               loss: float,
               losses: Dict[str, float],
               alpha_inter_ratio: float):
        """_summary_ Report to wandb and console

        Args:
            loss (float): _description_
            losses (Dict[str, float]): _description_
            alpha_inter_ratio (float): _description_
        """
        # Create an empty dict and update the dict with losses
        log_dict = {}
        log_dict.update({'Loss/loss': loss, "step": self.iter_step})

        # Log for each iter_step
        if losses is not None:
            log_dict.update({'depth_loss': losses['depth_loss'].mean()})
            log_dict.update({'sparse_loss': losses['sparse_loss'].mean()})
            log_dict.update({'sdf_mean': losses['sdf_mean'].mean()})
            if losses['color_fine_loss'] is not None:
                log_dict.update({'color_loss': losses['color_fine_loss'].mean()})
            if losses['psnr'] is not None:
                log_dict.update({'lod1/psnr': losses['psnr'].mean()})

            log_dict.update({'eik_loss': losses['gradient_error_loss'].mean()})
                
            # Log these lower parts as well
            log_dict.update({'variance': 1. / torch.exp(self.variance_network.variance * 10)})
            if losses['sparseness_1'] is not None:
                log_dict.update({'sparseness_0.01': losses['sparseness_1'].mean()})
            if losses['sparseness_2'] is not None:
                log_dict.update({'sparseness_0.02': losses['sparseness_2'].mean()})
            
        # Log it all in one part
        wandb.log(log_dict)

        print(self.base_exp_dir)
        print(
            'iter:{:8>d} '
            'loss = {:.4f} '
            'd_loss = {:.4f} '
            'color_loss = {:.4f} '
            'sparse_loss = {:.4f} '
            '  lr = {:.5f}, {:.5f}'.format(
                self.iter_step, loss,
                losses['depth_loss'].mean() if losses is not None else 0,
                losses['color_fine_loss'].mean() if losses is not None else 0,
                losses['sparse_loss'].mean() if losses is not None else 0,
                self.optimizer.param_groups[0]['lr'], 
                self.optimizer.param_groups[1]['lr']))

        print(colored('alpha_inter_ratio = {:.4f}\n'.format(
            alpha_inter_ratio), 'green'))

        if losses is not None:
            print(
                'iter:{:8>d} '
                'variance = {:.5f} '
                'weights_sum = {:.4f} '
                'weights_sum_fg = {:.4f} '
                'alpha_sum = {:.4f} '
                'sparse_weight= {:.4f} '
                'background_loss = {:.4f} '
                'background_weight = {:.4f} '
                    .format(
                    self.iter_step,
                    losses['variance'].mean(),
                    losses['weights_sum'].mean(),
                    losses['weights_sum_fg'].mean(),
                    losses['alpha_sum'].mean(),
                    losses['sparse_weight'].mean(),
                    losses['fg_bg_loss'].mean(),
                    losses['fg_bg_weight'].mean(),
                ))

    def adjust_learning_rate(self) -> None:
        # - ajust learning rate, cosine learning schedule
        learning_rate = (np.cos(np.pi * self.iter_step / self.end_iter) + 1.0) * 0.5 * 0.9 + 0.1
        learning_rate = self.learning_rate * learning_rate

        learning_rate_enc = (np.cos(np.pi * self.iter_step / self.end_iter) + 1.0) * 0.5 * 0.9 + 0.1
        learning_rate_enc = self.learning_rate_enc * learning_rate_enc

        for i, g in enumerate(self.optimizer.param_groups):
            # i==0 is the encoder!
            if i == 0:
                g['lr'] = learning_rate_enc
            else:
                g['lr'] = learning_rate


    def get_alpha_inter_ratio(self, 
                              start: float, 
                              end: float
                              ) -> float:
        if end == 0.0:
            return 1.0
        elif self.iter_step < start:
            return 0.0
        else:
            return np.min([1.0, (self.iter_step - start) / (end - start)])

    def file_backup(self) -> None:
        # copy python file
        dir_lis = self.conf['general.recording']
        
        # Check if recording exists and if it does, do not overwrite but save with different suffix
        path = os.path.join(self.base_exp_dir)
        recording_dirs = [path for path in os.listdir(path) if "recording" in path]

        if len(recording_dirs) != 0:
            if len(recording_dirs) == 1:
                recording_name = "recording_01"
            else:
                nums = sorted([int(name.split("_")[1]) for name in recording_dirs if len(name.split("_")) == 2])  
                recording_name = f"recording_{nums[-1]+1:02d}"
        else:
            recording_name = 'recording'

        os.makedirs(os.path.join(self.base_exp_dir, recording_name), exist_ok=False)
        for dir_name in dir_lis:
            cur_dir = os.path.join(self.base_exp_dir, recording_name, dir_name)
            os.makedirs(cur_dir, exist_ok=True)
            files = os.listdir(dir_name)
            for f_name in files:
                if f_name[-3:] == '.py':
                    copyfile(os.path.join(dir_name, f_name), os.path.join(cur_dir, f_name))

        # copy configs
        copyfile(self.conf_path, os.path.join(self.base_exp_dir, recording_name, 'config.conf'))


    def load_state_dict(self, 
                        network: torch.nn.Module, 
                        checkpoint: Any, 
                        comment: str
                        ) -> None:
            if network is not None:
                try:
                    pretrained_dict = checkpoint[comment]

                    model_dict = network.state_dict()

                    # 1. filter out unnecessary keys
                    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
                    # 2. overwrite entries in the existing state dict
                    try:
                        for key in pretrained_dict.keys():
                            if pretrained_dict[key].shape != model_dict[key].shape:
                                if "conv" in key:
                                    if (pretrained_dict[key].shape[0] != model_dict[key].shape[0] 
                                        and pretrained_dict[key].shape[1:] == model_dict[key].shape[1:]): 
                                        pretrained_dict[key] = pretrained_dict[key][int(model_dict[key].shape[0])]
                                        print(colored(f"Almost Impossible Shape mismatch for {key}, Loading only partial first dim", 'yellow'))
                                    elif len(pretrained_dict[key].shape) > 2:
                                        if (pretrained_dict[key].shape[0] != model_dict[key].shape[0]
                                            and pretrained_dict[key].shape[1] != model_dict[key].shape[1]
                                            and pretrained_dict[key].shape[0] == pretrained_dict[key].shape[1]
                                            and model_dict[key].shape[0] == model_dict[key].shape[1]
                                            and pretrained_dict[key].shape[2:] == model_dict[key].shape[2:]):
                                                pretrained_dict[key] = pretrained_dict[key][int(model_dict[key].shape[0]), int(model_dict[key].shape[1])]
                                                print(colored(f"Almost Impossible Shape mismatch for {key}, Loading only partial first 2 dims", 'yellow'))
                                        else:
                                            print(pretrained_dict[key].shape)
                                            print(model_dict[key].shape)
                                            pretrained_dict[key] = model_dict[key]
                                            print(colored(f"Impossible Shape mismatch for {key}, not loading", 'red'))
                                    else:
                                        print(pretrained_dict[key].shape)
                                        print(model_dict[key].shape)
                                        pretrained_dict[key] = model_dict[key]
                                        print(colored(f"Impossible Shape mismatch for {key}, not loading", 'red'))
                                if "attn" in key:
                                    if len(pretrained_dict[key].shape) > 1:
                                        if (pretrained_dict[key].shape[0] != model_dict[key].shape[0]
                                            and pretrained_dict[key].shape[1] != model_dict[key].shape[1]
                                            and pretrained_dict[key].shape[0] == pretrained_dict[key].shape[1]
                                            and model_dict[key].shape[0] == model_dict[key].shape[1]):
                                            if len(pretrained_dict[key].shape) > 2:
                                                if pretrained_dict[key].shape[2:] != model_dict[key].shape[2:]:
                                                    print(pretrained_dict[key].shape)
                                                    print(model_dict[key].shape)
                                                    pretrained_dict[key] = model_dict[key]
                                                    print(colored(f"Impossible Shape mismatch for {key}, not loading", 'red'))
                                                else:
                                                    pretrained_dict[key] = pretrained_dict[key][int(model_dict[key].shape[0]), int(model_dict[key].shape[1])]
                                                    print(colored(f"Almost Impossible Shape mismatch for {key}, Loading only partial first 2 dims", 'yellow'))
                                            else:
                                                pretrained_dict[key] = pretrained_dict[key][int(model_dict[key].shape[0]), int(model_dict[key].shape[1])]
                                                print(colored(f"Almost Impossible Shape mismatch for {key}, Loading only partial first 2 dims", 'yellow'))
                                        else:
                                            print(pretrained_dict[key].shape)
                                            print(model_dict[key].shape)
                                            pretrained_dict[key] = model_dict[key]
                                            print(colored(f"Impossible Shape mismatch for {key}, not loading", 'red'))
                                    else:
                                        pretrained_dict[key] = pretrained_dict[key][int(model_dict[key].shape[0])]
                                        print(colored(f"Almost Impossible Shape mismatch for {key}, Loading only partial first dim", 'yellow'))

                        model_dict.update(pretrained_dict)
                    except Exception as e:
                        print(e)
                    # 3. load the new state dict
                    # print(comment)
                    # breakpoint()
                    message = network.load_state_dict(pretrained_dict, strict=False)
                    print(colored(comment + " load successful", 'green'))
                except:
                    print(colored(comment + " load fails", 'yellow'))

    def load_checkpoint(self, 
                        checkpoint_name: str
                        ) -> None:
        checkpoint = torch.load(os.path.join(self.base_exp_dir, 'checkpoints', checkpoint_name),
                                map_location=self.device)

        # breakpoint()

        self.load_state_dict(self.encoder, checkpoint, 'encoder')
        self.load_state_dict(self.compress_layer, checkpoint, 'compress_layer')
        self.load_state_dict(self.sdf_network, checkpoint, 'sdf_network')

        self.load_state_dict(self.variance_network, checkpoint, 'variance_network')
        self.load_state_dict(self.rendering_network, checkpoint, 'rendering_network')

        if self.is_continue:
            try:
                msg = self.optimizer.load_state_dict(checkpoint['optimizer'])
                print(colored("optimizer load successful", 'green'))
                for state in self.optimizer.state.values():
                    for k, v in state.items():
                        if torch.is_tensor(v):
                            state[k] = v.to(self.device)
            except:
                print(colored("load optimizer fails", "yellow"))
            self.iter_step = checkpoint['iter_step'] if 'iter_step' in checkpoint.keys() else 0
            self.val_step = checkpoint['val_step'] if 'val_step' in checkpoint.keys() else 0

        print(colored(f"Pretrained model loaded from {checkpoint_name}.", "green"))
        self.logger.info('End')


    def save_state_dict(self, 
                        network: torch.nn.Module, 
                        checkpoint: Any, 
                        comment: str
                        ) -> None:
            if network is not None:
                checkpoint[comment] = network.state_dict()

    def save_checkpoint(self) -> None:
        checkpoint = {
            'optimizer': self.optimizer.state_dict(),
            'iter_step': self.iter_step,
            'val_step': self.val_step,
        }

        self.save_state_dict(self.sdf_network, checkpoint, "sdf_network")
        self.save_state_dict(self.compress_layer, checkpoint, "compress_layer")
        self.save_state_dict(self.rendering_network, checkpoint, "rendering_network")
        self.save_state_dict(self.variance_network, checkpoint, 'variance_network')

        self.save_state_dict(self.encoder, checkpoint, 'encoder')

        os.makedirs(os.path.join(self.base_exp_dir, 'checkpoints'), exist_ok=True)
        torch.save(checkpoint,
                   os.path.join(self.base_exp_dir, 'checkpoints', 'ckpt_{:0>6d}.pth'.format(self.iter_step)))
        

    def validate(self, 
                 idx: int = -1
                 ) -> None:
        # validate image

        ic(self.iter_step, idx)
        self.logger.info('Validate begin')

        if idx < 0:
            idx = self.val_step
        self.val_step += 1

        try:
            batch = next(self.val_dataloader_iterator)
        except:
            self.val_dataloader_iterator = iter(self.val_dataloader)  # reset
            batch = next(self.val_dataloader_iterator)

        background_rgb = None
        if self.use_white_bkgd:
            background_rgb = torch.ones([1, 3]).to(self.device)

        batch['batch_idx'] = torch.tensor([x for x in range(self.batch_size)])

        # - warmup params
        alpha_inter_ratio = self.get_alpha_inter_ratio(self.anneal_start, self.anneal_end)
        
        self.trainer(
            batch,
            background_rgb=background_rgb,
            alpha_inter_ratio=alpha_inter_ratio,  
            iter_step=self.iter_step,
            save_vis=True,
            mode='val',
        )


if __name__ == '__main__':
    # torch.set_default_tensor_type('torch.cuda.FloatTensor')
    torch.set_default_dtype(torch.float32)
    FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
    logging.basicConfig(level=logging.INFO, format=FORMAT)

    parser = argparse.ArgumentParser()
    parser.add_argument('--run_name', type=str, default='readability')
    parser.add_argument('--conf', type=str, default='./confs/base.conf')
    parser.add_argument('--mode', type=str, default='train')
    parser.add_argument('--wandb_mode', type=str, choices=["offline", "online", "disabled"], default="offline")
    parser.add_argument('--threshold', type=float, default=0.0)
    parser.add_argument('--is_continue', default=False, action="store_true")
    parser.add_argument('--is_restore', default=False, action="store_true")
    parser.add_argument('--is_finetune', default=False, action="store_true")
    parser.add_argument('--train_from_scratch', default=False, action="store_true")
    parser.add_argument('--local_rank', type=int, default=0)
    args = parser.parse_args()

    torch.cuda.set_device(args.local_rank)
    torch.backends.cudnn.benchmark = True  # ! make training 2x faster

    runner = Runner(args.conf, args.mode, args.is_continue, args.is_restore, args.local_rank)
    
    # Get Nice WandB config
    conf = args.__dict__
    conf.update(runner.conf.__dict__)
    conf.update(conf["history"])
    new_conf = {}
    for key, val in conf.items():
        if isinstance(val, dict):
            for k, v in val.items():
                if isinstance(v, dict):
                    for kk, vv in v.items():
                        new_conf[f"{key}.{k}.{kk}"] = vv
                elif isinstance(v, list):
                    for i, vv_l in enumerate(v):
                        if isinstance(vv_l, ConfigTree):
                            for kkk, vvv in vv_l.items():
                                if isinstance(vvv, ConfigTree):
                                    for kkkk, vvvv in vvv.items():
                                        new_conf[f"{key}.{k}.{kkk}.{kkkk}"] = vvvv
                                else:
                                    new_conf[f"{key}.{k}.{kkk}"] = vvv    
                        else:
                            new_conf[f"{key}.{k}.{i}"] = vv
                else:
                    new_conf[f"{key}.{k}"] = v
        else:
            new_conf[key] = val

    wandb.init(name=args.run_name, project='MatchSDF', entity="matchsdf", mode=args.wandb_mode, config=new_conf)

    if args.mode == 'train':
        runner.train()
    elif args.mode == 'val':
        for i in range(len(runner.val_dataset)):
            runner.validate()
